import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:table_sticky_headers/table_sticky_headers.dart';
import 'package:thesis/models/user_timetable.dart';

// ignore: must_be_immutable
class OverallTimetable extends StatefulWidget {

  List<User_timetable> monSubjectList = [];
  List<User_timetable> tueSubjectList = [];
  List<User_timetable> wedSubjectList = [];
  List<User_timetable> thuSubjectList = [];
  List<User_timetable> friSubjectList = [];
  List<User_timetable> satSubjectList = [];
  List<User_timetable> sunSubjectList = [];

  OverallTimetable(this.monSubjectList, this.tueSubjectList, this.wedSubjectList,
      this.thuSubjectList, this.friSubjectList, this.satSubjectList, this.sunSubjectList
      );
  @override
  _OverallTimetablePageState createState() =>
      _OverallTimetablePageState(monSubjectList, tueSubjectList, wedSubjectList,
          thuSubjectList, friSubjectList, satSubjectList, sunSubjectList);

}

class _OverallTimetablePageState extends State<OverallTimetable> {

  List<List<Widget>> dayTime = [];
  List<Widget> dayList = [];
  List<Widget> timeList = [];
  List<String> timeCheck = [];
  List<User_timetable> monSubjectList = [];
  List<User_timetable> tueSubjectList = [];
  List<User_timetable> wedSubjectList = [];
  List<User_timetable> thuSubjectList = [];
  List<User_timetable> friSubjectList = [];
  List<User_timetable> satSubjectList = [];
  List<User_timetable> sunSubjectList = [];

  String _days (int index){
    switch(index){
      case 0: return "Mon"; break;
      case 1: return "Tue"; break;
      case 2: return "Wed"; break;
      case 3: return "Thu"; break;
      case 4: return "Fri"; break;
      case 5: return "Sat"; break;
      case 6: return "Sun"; break;
      default: return ""; break;
    }
  }
  _OverallTimetablePageState(this.monSubjectList, this.tueSubjectList, this.wedSubjectList,
      this.thuSubjectList, this.friSubjectList, this.satSubjectList, this.sunSubjectList
      );

  //color: Hexcolor("#F1F57B"), mon
  //color: Hexcolor("#FECFFF"), tue
  //color: Hexcolor("#BDFF82"), wed
  //color: Hexcolor("#FFA874"), thu
  //color: Hexcolor("#9BE7FF"), fri
  //color: Hexcolor("#C988F7"), sat
  //color: Hexcolor("#FF7774"), sun
  setMonSubjectList(List<User_timetable> tempList){
    monSubjectList = tempList;
  }
  setMonTimetable(List<User_timetable> timeList, BuildContext context) {

    for(int index = 0; index < timeList.length; index++) {
      if(timeList[index].subject_id == "None"){
        continue;
      }
      int timeStart = getIndexOfTime(timeList[index].time_start);
      int timeEnd = getIndexOfTime(timeList[index].time_end);
      String subjectName = timeList[index].subject_id + " | " + timeList[index].subject_name;
      for (int i = timeStart; i < timeEnd; i++) {
        if(i == timeStart){
          dayTime[i][0] = Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.black,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#F1F57B"),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      subjectName.substring(0,5),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        else if(i == timeEnd-1){
          dayTime[i][0] = Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 10,
                      height: 100,
                      color: Colors.white,
                    ),
                    Container(
                      width: 110,
                      height: 100,
                      color: Hexcolor("#F1F57B"),
                    ),
                  ],
                ),
                Container(
                  width: 100,
                  height: 10,
                  color: Colors.white,
                ),
              ],
            ),
          );
        }
        else{
          dayTime[i][0] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.white,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#F1F57B"),
                ),
              ],
            ),
          );
        }
      }

    }
  }
  setTueSubjectList(List<User_timetable> tempList){
    tueSubjectList = tempList;
  }
  setTueTimetable(List<User_timetable> timeList, BuildContext context) {

    for(int index = 0; index < timeList.length; index++) {
      if(timeList[index].subject_id == "None"){
        continue;
      }
      int timeStart = getIndexOfTime(timeList[index].time_start);
      int timeEnd = getIndexOfTime(timeList[index].time_end);
      String subjectName = timeList[index].subject_id + " | " + timeList[index].subject_name;
      for (int i = timeStart; i < timeEnd; i++) {
        if(i == timeStart){
          dayTime[i][1] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.black,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#FECFFF"),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      subjectName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        else if(i == timeEnd-1){
          dayTime[i][1] = Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 10,
                      height: 100,
                      color: Colors.white,
                    ),
                    Container(
                      width: 110,
                      height: 100,
                      color: Hexcolor("#FECFFF"),
                    ),
                  ],
                ),
                Container(
                  width: 100,
                  height: 10,
                  color: Colors.white,
                ),
              ],
            ),
          );
        }
        else{
          dayTime[i][1] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.white,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#FECFFF"),
                ),
              ],
            ),
          );
        }
      }

    }
  }
  setWedSubjectList(List<User_timetable> tempList){
    wedSubjectList = tempList;
  }
  setWedTimetable(List<User_timetable> timeList, BuildContext context) {

    for(int index = 0; index < timeList.length; index++) {
      if(timeList[index].subject_id == "None"){
        continue;
      }
      int timeStart = getIndexOfTime(timeList[index].time_start);
      int timeEnd = getIndexOfTime(timeList[index].time_end);
      String subjectName = timeList[index].subject_id + " | " + timeList[index].subject_name;
      for (int i = timeStart; i < timeEnd; i++) {
        if(i == timeStart){
          dayTime[i][2] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.black,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#BDFF82"),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      subjectName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        else if(i == timeEnd-1){
          dayTime[i][2] = Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 10,
                      height: 100,
                      color: Colors.white,
                    ),
                    Container(
                      width: 110,
                      height: 100,
                      color: Hexcolor("#BDFF82"),
                    ),
                  ],
                ),
                Container(
                  width: 100,
                  height: 10,
                  color: Colors.white,
                ),
              ],
            ),
          );
        }
        else{
          dayTime[i][2] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.white,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#BDFF82"),
                ),
              ],
            ),
          );
        }
      }

    }
  }
  setThuSubjectList(List<User_timetable> tempList){
    thuSubjectList = tempList;
  }
  setThuTimetable(List<User_timetable> timeList, BuildContext context) {

    for(int index = 0; index < timeList.length; index++) {
      if(timeList[index].subject_id == "None"){
        continue;
      }
      int timeStart = getIndexOfTime(timeList[index].time_start);
      int timeEnd = getIndexOfTime(timeList[index].time_end);
      String subjectName = timeList[index].subject_id + " | " + timeList[index].subject_name;
      for (int i = timeStart; i < timeEnd; i++) {
        if(i == timeStart){
          dayTime[i][3] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.black,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#FFA874"),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      subjectName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        else if(i == timeEnd-1){
          dayTime[i][3] = Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 10,
                      height: 100,
                      color: Colors.white,
                    ),
                    Container(
                      width: 110,
                      height: 100,
                      color: Hexcolor("#FFA874"),
                    ),
                  ],
                ),
                Container(
                  width: 100,
                  height: 10,
                  color: Colors.white,
                ),
              ],
            ),
          );
        }
        else{
          dayTime[i][3] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.white,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#FFA874"),
                ),
              ],
            ),
          );
        }
      }

    }
  }
  setFriSubjectList(List<User_timetable> tempList){
    friSubjectList = tempList;
  }
  setFriTimetable(List<User_timetable> timeList, BuildContext context) {

    for(int index = 0; index < timeList.length; index++) {
      if(timeList[index].subject_id == "None"){
        continue;
      }
      int timeStart = getIndexOfTime(timeList[index].time_start);
      int timeEnd = getIndexOfTime(timeList[index].time_end);
      String subjectName = timeList[index].subject_id + " | " + timeList[index].subject_name;
      for (int i = timeStart; i < timeEnd; i++) {
        if(i == timeStart){
          dayTime[i][4] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.black,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#9BE7FF"),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      subjectName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        else if(i == timeEnd-1){
          dayTime[i][4] = Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 10,
                      height: 100,
                      color: Colors.white,
                    ),
                    Container(
                      width: 110,
                      height: 100,
                      color: Hexcolor("#9BE7FF"),
                    ),
                  ],
                ),
                Container(
                  width: 100,
                  height: 10,
                  color: Colors.white,
                ),
              ],
            ),
          );
        }
        else{
          dayTime[i][4] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.white,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#9BE7FF"),
                ),
              ],
            ),
          );
        }
      }

    }
  }
  setSatSubjectList(List<User_timetable> tempList){
    satSubjectList = tempList;
  }
  setSatTimetable(List<User_timetable> timeList, BuildContext context) {

    for(int index = 0; index < timeList.length; index++) {
      if(timeList[index].subject_id == "None"){
        continue;
      }
      int timeStart = getIndexOfTime(timeList[index].time_start);
      int timeEnd = getIndexOfTime(timeList[index].time_end);
      String subjectName = timeList[index].subject_id + " | " + timeList[index].subject_name;
      for (int i = timeStart; i < timeEnd; i++) {
        if(i == timeStart){
          dayTime[i][5] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.black,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#C988F7"),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      subjectName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        else if(i == timeEnd-1){
          dayTime[i][5] = Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 10,
                      height: 100,
                      color: Colors.white,
                    ),
                    Container(
                      width: 110,
                      height: 100,
                      color: Hexcolor("#C988F7"),
                    ),
                  ],
                ),
                Container(
                  width: 100,
                  height: 10,
                  color: Colors.white,
                ),
              ],
            ),
          );
        }
        else{
          dayTime[i][5] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.white,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#C988F7"),
                ),
              ],
            ),
          );
        }
      }

    }
  }
  setSunSubjectList(List<User_timetable> tempList){
    sunSubjectList = tempList;
  }
  setSunTimetable(List<User_timetable> timeList, BuildContext context) {

    for(int index = 0; index < timeList.length; index++) {
      if(timeList[index].section_id == null || timeList[index].time_start == "???"){
        continue;
      }
      int timeStart = getIndexOfTime(timeList[index].time_start);
      int timeEnd = getIndexOfTime(timeList[index].time_end);
      print("timestart: $timeStart - timeEnd: $timeEnd");
      String subjectName = timeList[index].subject_id + " | " + timeList[index].subject_name;
      for (int i = timeStart; i < timeEnd; i++) {
        if(i == timeStart){
          dayTime[i][6] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.black,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#FF7774"),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      subjectName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        else if(i == timeEnd-1){
          dayTime[i][6] = Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 10,
                      height: 100,
                      color: Colors.white,
                    ),
                    Container(
                      width: 110,
                      height: 100,
                      color: Hexcolor("#FF7774"),
                    ),
                  ],
                ),
                Container(
                  width: 100,
                  height: 10,
                  color: Colors.white,
                ),
              ],
            ),
          );
        }
        else{
          dayTime[i][6] = Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: 10,
                  height: 100,
                  color: Colors.white,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Hexcolor("#FF7774"),
                ),
              ],
            ),
          );
        }
      }

    }
  }

  int getIndexOfTime(String timeCheck){
    switch (timeCheck) {
      case "6.00": return 0; break;
      case "6.30": return 1; break;
      case "7.00": return 2; break;
      case "7.30": return 3; break;
      case "8.00": return 4; break;
      case "8.30": return 5; break;
      case "9.00": return 6; break;
      case "9.30": return 7; break;
      case "10.00": return 8; break;
      case "10.30": return 9; break;
      case "11.00": return 10; break;
      case "11.30": return 11; break;
      case "12.00": return 12; break;
      case "12.30": return 13; break;
      case "13.00": return 14; break;
      case "13.30": return 15; break;
      case "14.00": return 16; break;
      case "14.30": return 17; break;
      case "15.00": return 18; break;
      case "15.30": return 19; break;
      case "16.00": return 20; break;
      case "16.30": return 21; break;
      case "17.00": return 22; break;
      case "17.30": return 23; break;
      case "18.00": return 24; break;
      default: return -1; break;
    }
  }
  String getTimeOfIndex(int timeCheck){
    switch (timeCheck) {
      case 0: return "6.00"; break;
      case 1: return "6.30"; break;
      case 2: return "7.00"; break;
      case 3: return "7.30"; break;
      case 4: return "8.00"; break;
      case 5: return "8.30"; break;
      case 6: return "9.00"; break;
      case 7: return "9.30"; break;
      case 8: return "10.00"; break;
      case 9: return "10.30"; break;
      case 10: return "11.00"; break;
      case 11: return "11.30"; break;
      case 12: return "12.00"; break;
      case 13: return "12.30"; break;
      case 14: return "13.00"; break;
      case 15: return "13.30"; break;
      case 16: return "14.00"; break;
      case 17: return "14.30"; break;
      case 18: return "15.00"; break;
      case 19: return "15.30"; break;
      case 20: return "16.00"; break;
      case 21: return "16.30"; break;
      case 22: return "17.00"; break;
      case 23: return "17.30"; break;
      case 24: return "18.00"; break;
      default: return ""; break;
    }
  }

  @override
  void initState() {
    super.initState();

    for (int i = 0; i < 25; i++) {
      timeList.add(Container(
        height: 100,
        width: 100,
        child: Text(
          getTimeOfIndex(i),
          style: TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.w900),
        )
      )
      );
      List<Container> day = [];
      for(int j = 0; j < 7; j++){
        day.add(
          Container(child: Text("")
          ),
        );
      }
      dayTime.add(day);
    }
    for(int day = 0; day < 7; day++){
      dayList.add(Container(
        width: 100,
        height: 100,
        alignment: Alignment.center,
        child: Text(
          _days(day),
          style: TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.w900),
        ),
      ),
      );
    }
    setTimeCheck();
  }

  setTimeCheck() {
    for (int i = 0; i < 25; i++) {
      switch (i) {
        case 0:
          timeCheck.add("6:00");
          break;
        case 1:
          timeCheck.add("6:30");
          break;
        case 2:
          timeCheck.add("7:00");
          break;
        case 3:
          timeCheck.add("7:30");
          break;
        case 4:
          timeCheck.add("8:00");
          break;
        case 5:
          timeCheck.add("8:30");
          break;
        case 6:
          timeCheck.add("9:00");
          break;
        case 7:
          timeCheck.add("9:30");
          break;
        case 8:
          timeCheck.add("10:00");
          break;
        case 9:
          timeCheck.add("10:30");
          break;
        case 10:
          timeCheck.add("11:00");
          break;
        case 11:
          timeCheck.add("11:30");
          break;
        case 12:
          timeCheck.add("12:00");
          break;
        case 13:
          timeCheck.add("12:30");
          break;
        case 14:
          timeCheck.add("13:00");
          break;
        case 15:
          timeCheck.add("13:30");
          break;
        case 16:
          timeCheck.add("14:00");
          break;
        case 17:
          timeCheck.add("14:30");
          break;
        case 18:
          timeCheck.add("15:00");
          break;
        case 19:
          timeCheck.add("15:30");
          break;
        case 20:
          timeCheck.add("16:00");
          break;
        case 21:
          timeCheck.add("16:30");
          break;
        case 22:
          timeCheck.add("17:00");
          break;
        case 23:
          timeCheck.add("17:30");
          break;
        case 24:
          timeCheck.add("18:00");
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    setMonTimetable(monSubjectList, context);
    setTueTimetable(tueSubjectList, context);
    setWedTimetable(wedSubjectList, context);
    setThuTimetable(thuSubjectList, context);
    setFriTimetable(friSubjectList, context);
    setSatTimetable(satSubjectList, context);
    setSunTimetable(sunSubjectList, context);
    return StickyHeadersTable(
      columnsLength: dayList.length,
      rowsLength: dayTime.length,
      columnsTitleBuilder: (i) => dayList[i],
      rowsTitleBuilder: (i) => timeList[i],
      contentCellBuilder: (i, j) => dayTime[j][i],
      legendCell: Text('Time/Day', style: TextStyle(
          fontSize: 15.0, fontWeight: FontWeight.w900),),
    );
  }
}