class Credits {
  String group_id;
  String name_group;
  int all_credit;
  String curriculum;

  Credits(this.group_id, this.name_group, this.all_credit, this.curriculum);

  Credits.map(dynamic obj) {
    this.group_id = obj['group_id'];
    this.name_group = obj['name_group'];
    this.all_credit = obj['all_credit'];
    this.curriculum = obj['curriculum'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["group_id"] = group_id;
    map["name_group"] = name_group;
    map["all_credit"] = all_credit;
    map["curriculum"] = curriculum;
    return map;
  }

  @override
  String toString() {
    // TODO: implement toString
    return group_id + " " + name_group + " $all_credit " + curriculum + " ";
  }
}
