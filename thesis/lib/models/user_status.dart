class User_status {
  int id;
  String student_id;
  String subject_id;
  int status;
  String curriculum;
  String group_id;
//  String gpa;

  User_status(this.student_id, this.subject_id, this.status, this.curriculum, this.group_id);

  setID(int id){
    this.id = id;
  }

  User_status.map(dynamic obj) {
    this.id = obj['id'];
    this.student_id = obj['student_id'];
    this.subject_id = obj['subject_id'];
    this.status = obj['status'];
    this.curriculum = obj['curriculum'];
    this.group_id = obj['group_id'];
//    this.gpa = obj['gpa'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["student_id"] = student_id;
    map["subject_id"] = subject_id;
    map["status"] = status;
    map["curriculum"] = curriculum;
    map["group_id"] = group_id;
//    map['gpa'] = gpa;
    return map;
  }

  @override
  String toString() {
    return "SOS." ;
  }
}
