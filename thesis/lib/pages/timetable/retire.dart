import 'package:flutter/material.dart';
import 'package:thesis/pages/main/menuList.dart';

class Retire extends StatelessWidget {

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: true,
            //`true` if you want Flutter to automatically add Back Button when needed,
            //or `false` if you want to force your own back button every where
            title: Text('Warning!'),
            leading: IconButton(icon:Icon(Icons.arrow_back),
              onPressed:() {
                Navigator.of(context)
                    .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
                return new MenuList();
                }));
              },
            )
        ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/bg.png'),
          fit: BoxFit.cover),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('You have to study over 8 years...', style: TextStyle(fontSize: 40)),
              SizedBox(
                height: 50,
              ),
            ]
    ))));
  }
}