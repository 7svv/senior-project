import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:path/path.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:thesis/models/credit_group.dart';
import 'package:thesis/models/user.dart';
import 'package:thesis/models/user_timetable.dart';
import 'package:thesis/pages/login/login_page.dart';
import 'package:thesis/pages/main/allList.dart';
import 'package:thesis/pages/main/completedList.dart';
import 'package:thesis/pages/main/YearTermSelector.dart';
import 'package:thesis/pages/main/incompletedList.dart';
import 'package:thesis/pages/timetable/timetabling.dart';
import 'package:thesis/sizeconfig.dart';
import 'editProfile.dart';
import 'currentTime.dart';

String curStudent = User.studentId;

var vertical = SizeConfig.safeBlockVertical;
var horizontal = SizeConfig.blockSizeHorizontal;
var fontSize = SizeConfig.blockSizeVertical;

setStudentMenuList(String student_id){
  curStudent = student_id;
}

class MenuList extends StatefulWidget {
  MenuList({Key key}) : super(key: key);
  @override
  _MenuList createState() => _MenuList();
}

class _MenuList extends State<MenuList> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _overlayKey = GlobalKey<FormState>();
  List<User_timetable> allTimetable = [];
  List<AllGroupCredits> allGroupCredits = [];
//  List<Timetable> termTimetable = [];

  Future getAllTimetable(int semester) async {
    print("getAllTimetable...");
    var dbHelper = DatabaseHelper();
    var tempTimetable = await dbHelper.getTimetable(curStudent,semester);
    var tempGroupCredits = await dbHelper.getAllCreditGroups();;

    setState(() {
      allTimetable = tempTimetable;
      allGroupCredits = tempGroupCredits;
    });
  }

  creatAlertDialog(BuildContext context){
    TextEditingController customController =  TextEditingController();
    return showDialog(context: context, builder: (context){
      return AlertDialog(
        title: Text("Choose Your Semester",style: TextStyle(color: Colors.black),),
        content: Container(
          //height: 90,
          child: YearTermSelector(),
        )
        ,
        actions: <Widget>[
          MaterialButton(
            color: Colors.blueAccent,
            elevation: 5.0,
            child: Text("CONFIRM"),
            onPressed: (){
              var yearTermSelected = getYearTerm();
              setUserTimetable(curStudent);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (context){
                        return OverlaySupport(child: MaterialApp(
                          title: "Overlay Support.",
                          key: _overlayKey,
                          home: Timetabling(allTimetable: allTimetable, allGroupCredits: allGroupCredits, currYearTerm: yearTermSelected),
                        ));
                      }
                  ),
                  ModalRoute.withName("/Timetabling")
              );
            },
          )
        ],
      );
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllTimetable(0);
  }

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
      InCompletedList(),
      CompletedList(),
      AllList()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<User> getUserByStudent_id(String student_id) async {
    var db = await DatabaseHelper();
    Future<User> curUser = db.getUser(student_id);
    return curUser;
  }

  @override
  Widget build(BuildContext context) {
    print("current Student: "+ curStudent);
    setStudentInCompletedList(curStudent);
    setStudentCompletedList(curStudent);
    setStudentAllList(curStudent);
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: const Text('Curriculums'),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      drawer: new Drawer(
        child: FutureBuilder<User>(
          future: getUserByStudent_id(curStudent),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: <Widget>[
                  DrawerHeader(
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerLeft,
                            child: ClipOval(
                              child: Container(
                                color: Colors.white,
                                height: 75.0, // height of the button
                                width: 75.0, // width of the button
                                child: Center(
                                  child: Text(
                                    snapshot.data.name.substring(0,1).toUpperCase()+snapshot.data.name.substring(1,2),
                                    style: TextStyle(color: Colors.blue, fontSize: 30),
                                  ),
                                ),
                              ),
                            )
                        ),
                        Align(
                          alignment: Alignment.centerLeft + Alignment(1.4,0),
                          child: Text(
                            snapshot.data.name,
                            style: TextStyle(color: Colors.white, fontSize: fontSize*2.5),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft + Alignment(1.2,.5),
                          child: Text(
                            snapshot.data.student_id,
                            style: TextStyle(color: Colors.white70, fontSize: fontSize*2),
                          ),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: Colors.blue,
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.edit, size: fontSize*3),
                    title: Text('Edit Profile', style: TextStyle(fontSize: fontSize*2)),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => editProfile()),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.calendar_today, size: fontSize*3),
                    title: Text('Current Timetable', style: TextStyle(fontSize: fontSize*2)),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => currentTime()),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.exit_to_app, size: fontSize*3),
                    title: Text('Logout', style: TextStyle(fontSize: fontSize*2)),
                    onTap: () {
                      setState(() {
                        User.studentId = null;
                      });
                      Navigator.push(context, new MaterialPageRoute(
                          builder: (context) =>
                          new LoginPage())
                      );
                    },
                  ),
                ],
              );
            }
            return new Container(
                alignment:
                AlignmentDirectional.center,
                padding: EdgeInsets.all(20),
                        child: new CircularProgressIndicator(),
            );
          }
       ),
      ),
      floatingActionButton: FloatingActionButton(
        child: IconButton(icon: Icon(Icons.today,color: Colors.white, size: fontSize*3.5)),
        onPressed:(){
          creatAlertDialog(context);
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.clear, size: fontSize*3.5),
            title: Text('Incompleted', style: TextStyle(fontSize: fontSize*2)),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.check, size: fontSize*3.5),
            title: Text('Completed', style: TextStyle(fontSize: fontSize*2)),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.description, size: fontSize*3.5),
            title: Text('All Subject', style: TextStyle(fontSize: fontSize*2)),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.pinkAccent,
        onTap: _onItemTapped,
      ),
    );
  }
}