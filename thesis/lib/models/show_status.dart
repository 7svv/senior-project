class Show_status {
  int id;
  String student_id;
  String subject_id;
  String subject_name;
  String curriculum;
  int status;
  String group_id;

  Show_status(this.id, this.student_id, this.subject_id, this.subject_name, this.curriculum, this.group_id, this.status);

  Show_status.map(dynamic obj) {
    this.id = obj['id'];
    this.student_id = obj['student_id'];
    this.subject_id = obj['subject_id'];
    this.subject_name = obj['subject_name'];
    this.curriculum = obj['curriculum'];
    this.group_id = obj['group_id'];
    this.status = obj['status'];
  }
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["student_id"] = student_id;
    map["subject_id"] = subject_id;
    map["subject_name"] = subject_name;
    map["curriculum"] = curriculum;
    map["group_id"] = group_id;
    map["status"] = status;
    return map;
  }

}
