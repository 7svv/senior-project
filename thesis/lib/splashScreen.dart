import 'package:flutter/material.dart';
import 'package:thesis/main.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Loading Screen',
        home: SplashScreen(
          'assets/spa_load_screen.flr',
          MyApp(),
          startAnimation: 'loading',
          backgroundColor: Color(0xff1372FF),
        )
    );
  }
}