import 'package:thesis/data/rest-data.dart';
import 'package:thesis/models/user.dart';

abstract class LoginPageContract{
  void onLoginSuccess(User user);
  void onLoginError(String error);
}

class LoginPagePresenter {
  LoginPageContract _view;
  RestData api = new RestData();
  LoginPagePresenter(this._view);

//Simulator login
  doLogin(String student_id, password){
    print("dolog id = " + student_id);
    User.studentId = student_id;
    api
    .login(student_id, password)
    .then((user) {
      _view.onLoginSuccess(user);
      print("onlog "+user.toString());
    })
    .catchError((onError) => _view.onLoginError(onError.toString()));

  }
}