import 'package:flutter/material.dart';
import 'package:thesis/Admin/Admin.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:thesis/pages/login/register.dart';
import 'package:thesis/pages/main/allList.dart';
import 'package:thesis/models/user.dart';
import 'package:thesis/pages/login/login_presenter.dart';
import 'package:thesis/pages/main/menuList.dart';
import 'package:thesis/sizeconfig.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> implements LoginPageContract {
  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _student_id, _password;
  // Initially password is obscure
  bool _obscureText = true;

  LoginPagePresenter _presenter;

  _LoginPageState() {
    _presenter = new LoginPagePresenter(this);
  }

  void _register() {
    var dbb = DatabaseHelper();
//    db.getCurr();
    dbb.allUser();
    Navigator.push(context, new MaterialPageRoute(
        builder: (context) =>
        new RegisterPage())
    );
  }

  void _submit() {
    final form = formKey.currentState;
    if (form.validate()) {
      setState(() {
        _isLoading = true;
        form.save();
        if(_student_id == "admin" && _password == "admin"){
          Navigator.push(context, new MaterialPageRoute(
              builder: (context) =>
              new AdminDB())
          );
        }
        _presenter.doLogin(_student_id, _password);
      });
    }
  }

  void _alluser() {
    var db = new DatabaseHelper();
    Future<List> alluser = db.allUser();
    print(alluser);
  }
  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    SizeConfig().init(context);
    var vertical = SizeConfig.safeBlockVertical;
    var horizontal = SizeConfig.blockSizeHorizontal;
    var fontSize = SizeConfig.blockSizeVertical;
    var loginBtn = new MaterialButton(
      padding: EdgeInsets.fromLTRB(horizontal*30,vertical*2,horizontal*30,vertical*2),
      onPressed: _submit,
      child: new Text(
          "LOGIN",
          style: TextStyle(fontSize: fontSize*3)
      ),
      color: Colors.blueAccent,
      textColor: Colors.white,
      highlightColor: Color(0xff1372FF),
      shape: StadiumBorder(),
      elevation: 8,
      highlightElevation: 2,

    );
    var registerBtn = new MaterialButton(
      onPressed: _register,
      textColor: Colors.black54,
      child: new Text(
          "Create new user?",
          style: TextStyle(fontSize: fontSize*2.5),
      ),
    );

    var loginForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.fromLTRB(0,vertical*15,0,0),
          child: new Image.asset(
            'assets/images/spa_logov1.png',
            height: vertical*25,
            width: horizontal*25,
            fit: BoxFit.cover,
          ),
        ),
        new Padding(
          padding: const EdgeInsets.fromLTRB(0,15,0,0),
          child: new Text('STUDYING PLAN ASSISTANT',
            style: TextStyle(
            color: Color(0xff1372FF),
            fontWeight: FontWeight.bold,
            fontSize: fontSize*2.8,
            ),
          )
        ),
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.fromLTRB(30,vertical*5,30,0),
                child: new TextFormField(
                  autofocus: true,
                  style: TextStyle(
                    color: Color(0xff1372FF),
                      fontSize: fontSize*2.5
                  ),
                  onSaved: (val) => _student_id = val,
                  decoration: new InputDecoration(
                    hintText: 'รหัสนักศึกษา',
//                    fillColor: Colors.white,
//                      filled: true,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(horizontal*10),
                        borderSide: BorderSide(color: Color(0xff1372FF)),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderRadius: BorderRadius.circular(horizontal*10),
//                        borderSide: BorderSide(color: Color(0xff1372FF)),
                      ),
                    labelText: "Student ID",
                    labelStyle: TextStyle(
                      fontSize: fontSize*2.5
                    ),
                    prefixIcon: new Icon(Icons.person,
                      size: fontSize*3.5,)
                  ),
                  validator: (String value) {
                    if (value.trim().isEmpty) {
                      return 'Student ID is required';
                    }
                    else
                      return null;
                  },
                ),
              ),
              new Padding(
                padding: EdgeInsets.fromLTRB(30,10,30,vertical*5),
                child: new TextFormField(
                  style: TextStyle(
                      color: Color(0xff1372FF),
                      fontSize: fontSize*2.5
                  ),
                  onSaved: (val) => _password = val,
                  decoration:  new InputDecoration(
                    hintText: 'รหัสผ่าน',
//                      fillColor: Colors.white,
//                      filled: true,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(horizontal*10),
                        borderSide: BorderSide(color: Color(0xff1372FF)),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderRadius: BorderRadius.circular(horizontal*10),
//                        borderSide: BorderSide(color: Color(0xff1372FF)),
                      ),
                    labelText: "Password",
                      labelStyle: TextStyle(
                          fontSize: fontSize*2.5
                      ),
                    suffixIcon: new GestureDetector(
                      onTap: () {
                      setState(() {
                      _obscureText = !_obscureText;
                      });
                      },
                      child:
                      new Icon(_obscureText ? Icons.visibility : Icons.visibility_off,
                        size: fontSize*3.5,),
                      ),
                    prefixIcon: new Icon(Icons.lock,
                      size: fontSize*3.5,)
                  ),
                  validator: (String value) {
                    if (value.trim().isEmpty) {
                      return 'Password is required';
                    }
                    else
                      return null;
                  },
                  autofocus: false,
                  obscureText: _obscureText,
                ),
              ),
            ],
          ),
        ),
        new Padding(
            padding: const EdgeInsets.all(10.0),
            child: loginBtn),

        registerBtn,
//        listAllBtn
      ],
    );

    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text("Login Page"),
//        backgroundColor: Color(0xff1372FF),
//      ),
      resizeToAvoidBottomPadding: false,
      key: scaffoldKey,
      body: new Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        decoration: new BoxDecoration(color: Colors.white),
        child: SingleChildScrollView(
          child: new Center(
            child: loginForm,
          ),
        )
      ),
    );
  }

  @override
  void onLoginError(String error) {
    print("On Error || Or Breaking Permission (Admin)");
    // TODO: implement onLoginError
    _showSnackBar("Login not successful");
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onLoginSuccess(User user) async {
    // TODO: implement onLoginSuccess
    if(user.student_id == ""){
      _showSnackBar("Login not successful");
    }else{
    _showSnackBar(user.toString() + " Not Successful");
    }
    setState(() {
      _isLoading = true;
    });
    if(user.flaglogged == "logged"){
      print("Logged");
      User.studentId = user.student_id;
      setStudentMenuList(user.student_id);
      Navigator.push(context, new MaterialPageRoute(
          builder: (context) =>
          new MenuList(key: UniqueKey()))
      );
    }else{
      print("Not Logged");
    }
  }
}
