import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:thesis/models/user_timetable.dart';

// ignore: must_be_immutable
class OverallTimetableTable extends StatefulWidget {

  List<User_timetable> planTimetable = [];

  OverallTimetableTable(this.planTimetable);
  @override
  _OverallTimetableTablePageState createState() =>
      _OverallTimetableTablePageState(planTimetable);

}

class _OverallTimetableTablePageState extends State<OverallTimetableTable> {

  List<User_timetable> planTimetable = [];
  List<String> checkRepeat = [];
  _OverallTimetableTablePageState(this.planTimetable);

  List<DataRow> allList = [];
  @override
  void initState() {
    super.initState();
  print("init overallTable");
  print(planTimetable);
  planTimetable.removeWhere((element) => element.subject_id == "None");
  for(int i = 0; i < planTimetable.length; i++){
    if(checkRepeat.contains(planTimetable[i].subject_id)){
      planTimetable.removeAt(i);
      i--;
    }
    else{
      checkRepeat.add(planTimetable[i].subject_id);
    }
  }

  planTimetable.sort((a, b) {
    var tempTimeB = b.time_start;
    var tempTimeA = a.time_start;
    if(b.time_start == "???"){
      tempTimeB = "19.00";
    }
    if(a.time_start == "???"){
      tempTimeA = "19.00";
    }
    var checkB = tempTimeB.split(".");
    int timeB = int.parse(checkB[0]);
    var checkA = tempTimeA.split(".");
    int timeA = int.parse(checkA[0]);
    return timeA.compareTo(timeB);
  });
    planTimetable.sort((a, b) {
      var tempDayB = b.day.substring(0,3);
      var tempDayA = a.day.substring(0,3);
      if(b.day == "???"){
        tempDayB = "???";
      }
      if(a.day == "???"){
        tempDayA = "???";
      }
      return checkDay(tempDayA).compareTo(checkDay(tempDayB));
    });

    for(int i = 0; i < planTimetable.length; i++){
      if(planTimetable[i].day.length > 3){
       var tempDay1 = getColor(planTimetable[i].day.substring(0,3));
       var tempDay2 = getColor(planTimetable[i].day.substring(4,7));
       allList.add(
         DataRow(
           cells: [
             DataCell(
                 Row(
                   children: <Widget>[
                     Container(
                       color: tempDay1,
                       width: 50,
                       alignment: Alignment.center,
                       child: Text(planTimetable[i].day.substring(0,3),),
                     ),
                     Container(
                       color: tempDay2,
                       width: 50,
                       alignment: Alignment.center,
                       child: Text(planTimetable[i].day.substring(4,7),),
                     )
                   ],
                 )
             ),
             DataCell(
                 Container(
                   alignment: Alignment.center,
                   child: Text((planTimetable[i].time_start != "19.00" ?  planTimetable[i].time_start:"???") + "-" + planTimetable[i].time_end,
                     textAlign: TextAlign.center,),
                 )
             ),
             DataCell(
                 Container(
                   alignment: Alignment.center,
                   child: Text(planTimetable[i].subject_id,
                     textAlign: TextAlign.center,),
                 )
             ),
             DataCell(
                 Container(
                   width: 160,
                   alignment: Alignment.center,
                   child: Text(planTimetable[i].subject_name,
                     overflow: TextOverflow.ellipsis,
                     textAlign: TextAlign.center,
                   ),
                 )
             ),
             DataCell(
                 Container(
                   alignment: Alignment.center,
                   child: Text(planTimetable[i].credit.toString(),
                     textAlign: TextAlign.center,
                   ),
                 )
             ),
           ],
           selected: true,
         ),
       );
      }
      else{
        if(planTimetable[i].day == "mon"){
          allList.add(
            DataRow(
              cells: [
                DataCell(
                    Container(
                      color: Hexcolor("#F1F57B"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].day,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#F1F57B"),
                      alignment: Alignment.center,
                      child: Text((planTimetable[i].time_start != "19.00" ?  planTimetable[i].time_start:"???") + "-" + planTimetable[i].time_end,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#F1F57B"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_id,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      width: 160,
                      color: Hexcolor("#F1F57B"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#F1F57B"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].credit.toString(),
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
              ],
              selected: true,
            ),
          );
        }
        else if(planTimetable[i].day == "tue"){
          allList.add(
            DataRow(
              cells: [
                DataCell(
                    Container(
                      color: Hexcolor("#FECFFF"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].day,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FECFFF"),
                      alignment: Alignment.center,
                      child: Text((planTimetable[i].time_start != "19.00" ?  planTimetable[i].time_start:"???") + "-" + planTimetable[i].time_end,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FECFFF"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_id,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      width: 160,
                      color: Hexcolor("#FECFFF"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FECFFF"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].credit.toString(),
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
              ],
              selected: true,
            ),
          );
        }
        else if(planTimetable[i].day == "wed"){
          allList.add(
            DataRow(
              cells: [
                DataCell(
                    Container(
                      color: Hexcolor("#BDFF82"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].day,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#BDFF82"),
                      alignment: Alignment.center,
                      child: Text((planTimetable[i].time_start != "19.00" ?  planTimetable[i].time_start:"???") + "-" + planTimetable[i].time_end,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#BDFF82"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_id,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      width: 160,
                      color: Hexcolor("#BDFF82"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#BDFF82"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].credit.toString(),
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
              ],
              selected: true,
            ),
          );
        }
        else if(planTimetable[i].day == "thu"){
          allList.add(
            DataRow(
              cells: [
                DataCell(
                    Container(
                      color: Hexcolor("#FFA874"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].day,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FFA874"),
                      alignment: Alignment.center,
                      child: Text((planTimetable[i].time_start != "19.00" ?  planTimetable[i].time_start:"???") + "-" + planTimetable[i].time_end,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FFA874"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_id,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      width: 160,
                      color: Hexcolor("#FFA874"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FFA874"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].credit.toString(),
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
              ],
              selected: true,
            ),
          );
        }
        else if(planTimetable[i].day == "fri"){
          allList.add(
            DataRow(
              cells: [
                DataCell(
                    Container(
                      color: Hexcolor("#9BE7FF"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].day,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#9BE7FF"),
                      alignment: Alignment.center,
                      child: Text((planTimetable[i].time_start != "19.00" ?  planTimetable[i].time_start:"???") + "-" + planTimetable[i].time_end,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#9BE7FF"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_id,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      width: 160,
                      color: Hexcolor("#9BE7FF"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#9BE7FF"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].credit.toString(),
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
              ],
              selected: true,
            ),
          );
        }
        else if(planTimetable[i].day == "sat"){
          allList.add(
            DataRow(
              cells: [
                DataCell(
                    Container(
                      color: Hexcolor("#C988F7"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].day,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#C988F7"),
                      alignment: Alignment.center,
                      child: Text((planTimetable[i].time_start != "19.00" ?  planTimetable[i].time_start:"???") + "-" + planTimetable[i].time_end,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#C988F7"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_id,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      width: 160,
                      color: Hexcolor("#C988F7"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#C988F7"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].credit.toString(),
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
              ],
              selected: true,
            ),
          );
        }
        else if(planTimetable[i].day == "sun" || planTimetable[i].day == "???"){
          allList.add(
            DataRow(
              cells: [
                DataCell(
                    Container(
                      color: Hexcolor("#FF7774"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].day,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FF7774"),
                      alignment: Alignment.center,
                      child: Text((planTimetable[i].time_start != "19.00" ?  planTimetable[i].time_start:"???") + "-" + planTimetable[i].time_end,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FF7774"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_id,
                        textAlign: TextAlign.center,),
                    )
                ),
                DataCell(
                    Container(
                      width: 160,
                      color: Hexcolor("#FF7774"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].subject_name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
                DataCell(
                    Container(
                      color: Hexcolor("#FF7774"),
                      alignment: Alignment.center,
                      child: Text(planTimetable[i].credit.toString(),
                        textAlign: TextAlign.center,
                      ),
                    )
                ),
              ],
              selected: true,
            ),
          );
        }
      }
    }
  }

  int checkDay(String day){
    switch(day){
      case "mon": return 0;
      case "tue": return 1;
      case "wed": return 2;
      case "thu": return 3;
      case "fri": return 4;
      case "sat": return 5;
      case "sun": return 6;
      default: return 10;
    }
  }
  Hexcolor getColor(String day){
    switch(day){
      case "mon": return Hexcolor("#F1F57B");
      case "tue": return Hexcolor("#FECFFF");
      case "wed": return Hexcolor("#BDFF82");
      case "thu": return Hexcolor("#FFA874");
      case "fri": return Hexcolor("#9BE7FF");
      case "sat": return Hexcolor("#C988F7");
      case "sun": return Hexcolor("#FF7774");
      default: return Hexcolor("#848489");
    }
  }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: FittedBox(
        child: DataTable(
          columnSpacing: 5,
          columns: [
            DataColumn(
                label: Container(
                  padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                  color: Colors.blue,
                  child: Text(
                    "Day",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                        fontSize: 12.0, fontWeight: FontWeight.w900),
                  ),
                )
            ),
            DataColumn(
                label: Container(
                  padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                  color: Colors.blue,
                  child: Text(
                    "Time",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12.0, fontWeight: FontWeight.w900),
                  ),
                )
                ),
            DataColumn(label: Container(
              padding: EdgeInsets.fromLTRB(30, 20, 30, 20),

              color: Colors.blue,
              child: Text(
                "SUBJECT ID",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 12.0, fontWeight: FontWeight.w900),
              ),
            )),
            DataColumn(label: Container(
              padding: EdgeInsets.fromLTRB(30, 20, 30, 20),

              color: Colors.blue,
              child: Text(
                "SUBJECT NAME",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 12.0, fontWeight: FontWeight.w900),
              ),
            )),
            DataColumn(label: Container(
              padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
              color: Colors.blue,
              child: Text(
                "CREDIT",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 12.0, fontWeight: FontWeight.w900),
              ),
            )),
          ],
          rows: [
            for(int i = 0; i < allList.length; i++)
              allList[i],
          ],
        ),
      ),
    );
  }
}