import 'package:flutter/material.dart';
import 'package:thesis/models/credit_group.dart';
import 'package:thesis/models/current_credits.dart';
import 'package:thesis/models/timetable.dart';
import 'package:thesis/models/user_timetable.dart';
import 'package:thesis/pages/timetable/allTimetable.dart';
import 'package:thesis/pages/timetable/retire.dart';
import 'package:thesis/sizeconfig.dart';
import 'package:thesis/models/global_vars.dart' as globals;

// ignore: must_be_immutable
class Confirmation extends StatefulWidget {
  Key key;
  String currYearTerm;
  List<User_timetable> allTimetable = [];
  List<AllGroupCredits> allGroupCredits = [];
  Timetable termInit;
  List<Timetable> termTimetable = [];
  int allCurrTermCredits;

  Confirmation(this.key, this.currYearTerm, this.termInit, this.allTimetable,
      this.allGroupCredits, this.allCurrTermCredits) {
    termTimetable = [];
    termTimetable.add(termInit);
  }

  @override
  _ConfirmationState createState() => _ConfirmationState(key: key);
}

class _ConfirmationState extends State<Confirmation> {
  _ConfirmationState({Key key});

  List<String> yearTerm = [
    "1-1",
    "1-2",
    "2-1",
    "2-2",
    "3-1",
    "3-2",
    "4-1",
    "4-2",
    "5-1",
    "5-2",
    "6-1",
    "6-2",
    "7-1",
    "7-2",
    "8-1",
    "8-2",
  ];

  Widget build(BuildContext context) {
    print("all credits: " + widget.allCurrTermCredits.toString());
    SizeConfig().init(context);
    var maxHeight = SizeConfig.screenHeight;
    var maxWidth = SizeConfig.screenWidth;
    var vertical = SizeConfig.safeBlockVertical;
    var horizontal = SizeConfig.blockSizeHorizontal;
    var fontSize = SizeConfig.blockSizeVertical;
    int allCurrTermCredits = globals.allCredits[0];

    if (widget.allCurrTermCredits > 22) {
      return Expanded(
        child: Align(
          alignment: FractionalOffset.bottomCenter,
          child: SizedBox(
            width: double.infinity,
            height: vertical*10,
            child: RaisedButton(
              padding: EdgeInsets.fromLTRB(
                  horizontal * 100, vertical * 2, horizontal * 100, vertical * 5),
              onPressed: () {},
              child: Text("CONFIRM", style: TextStyle(fontSize: fontSize * 10)),
              color: Colors.grey,
              textColor: Colors.white,
              elevation: 8,
              highlightElevation: 2,
            ),
          ),
        ),
      );
    } else {
      return Expanded(
        child: Align(
          alignment: FractionalOffset.bottomCenter,
          child: SizedBox(
            width: double.infinity,
            height: vertical*10,
            child: RaisedButton(
              onPressed: () {
                int i = 0;
                int startTerm = yearTerm.indexOf(widget.currYearTerm);
                widget.termTimetable[0].allCredits = widget.termTimetable[0]
                    .getCredits(globals.getAllDropdownVal(0));
                widget.termTimetable[0].setOverall(globals.getAllDropdownVal(0));
                while (true) {
                  if (i > yearTerm.length - 1) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Retire()),
                    );
                    print("Over 8 years.");
                    break;
                  }
                  if (i > 0) {
                    globals.allCredits.add(0);
                    globals.dayList.add([]);
                    globals.allCurrentCredits
                        .add(new CurrentCredits(UniqueKey(), 0));
                    widget.termTimetable.add(Timetable(
                        key: UniqueKey(),
                        allTimetable: widget.allTimetable,
                        yearTerm: yearTerm[startTerm + i],
                        allGroupCredits: widget.allGroupCredits,
                        indexList: i));
                  }
                  if (i == 1) {
                    List<User_timetable> tempPass =
                        widget.termTimetable[0].passSubjects;
                    tempPass.addAll(globals.getAllDropdownVal(0));
                    widget.termTimetable[i]
                        .setPassSubjects(globals.getAllDropdownVal(0), tempPass);
                  }
                  if (i > 1) {
                    widget.termTimetable[i].setPassSubjects(
                        widget.termTimetable[i - 1].planTimetable,
                        widget.termTimetable[i - 1].passSubjects);
                  }
                  if (widget.termTimetable[i].checkFinalPass) {
                    break;
                  }
//            print("$i plan term: " + termTimetable[i].planTimetable.toString());
//            print("$i pass term: " + termTimetable[i].passSubjects.toString());
//            print("$i group term: " + termTimetable[i].allGroupCredits.toString());
                  i++;
                }
                if (widget.termTimetable.last.allCredits == 0 &&
                    widget.termTimetable.last.planTimetable.isEmpty) {
                  widget.termTimetable.removeLast();
                }
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AllTimetable(
                          widget.currYearTerm, widget.termTimetable)),
                );
              },
              child:
                  new Text("CONFIRM", style: TextStyle(fontSize: fontSize * 3)),
              color: Colors.blue,
              textColor: Colors.white,
              highlightColor: Color(0xff002C70),
              elevation: 8,
              highlightElevation: 2,
            ),
          ),
        ),
      );
    }
  }
}
