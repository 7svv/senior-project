class AllGroupCredits {
  String group_id;
  String name_group;
  int currCredits = 0;
  int all_credit;
  String curriculum;

  AllGroupCredits(this.group_id, this.name_group, this.all_credit, this.curriculum);

  AllGroupCredits.map(dynamic obj) {
    this.group_id = obj['group_id'];
    this.name_group = obj['name_group'];
    this.all_credit = obj['all_credit'];
    this.curriculum = obj['curriculum'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["group_id"] = group_id;
    map["name_group"] = name_group;
    map["all_credit"] = all_credit;
    map["curriculum"] = curriculum;
    return map;
  }

  @override
  String toString() {
    return "Group: " + group_id + " currentCredits: $currCredits|$all_credit\n";
  }
}
