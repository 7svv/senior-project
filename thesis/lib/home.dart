import 'package:flutter/material.dart';
import 'etc.dart';

class HomeMain extends StatefulWidget {
  HomeScreen createState()=> HomeScreen();
}

class HomeScreen extends State<HomeMain> {
  String _faculty;
  String _course;
  bool fal = false;
  bool cou = false;

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/bg.png'),
          fit: BoxFit.cover),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('FACULTY', style: TextStyle(fontSize: 30)),
              DropdownButton<String>(
                value: _faculty,
                hint: Text('Select your Faculty ...'),
                items: <String>['Engineering', '...'].map((String val) {
                  return new DropdownMenuItem<String>(
                    value: val,
                    child: new Text(val),
                  );
                }).toList(),
                onChanged: (String newVal) {
                  fal = true;
                  setState(() {
                    _faculty = newVal;
                  });
                },
              ),
              SizedBox(
                height: 100,
              ),
              if(fal || cou)
                Text('COURSE', style: TextStyle(fontSize: 30)),
              if(fal || cou)
                DropdownButton<String>(
                  value: _course,
                  hint: Text('Select your Course ...'),
                  items: <String>['Computer Years:56', '...'].map((String value) {
                  return new DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                    );
                  }).toList(),
                  onChanged: (String newVal) {
                    cou = true;
                    setState(() {
                    _course = newVal;
                  });
                 },
                ),
                SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          children: <Widget>[
            if(!fal && !cou)
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
                    return new Etc();
                  }));
                },
                color: Colors.blue,
                child: const Text('Insert Faculty or Course',
                    style: TextStyle(fontSize: 27, color: Colors.white)),
              ),
            ),
            if(fal & !cou)
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: RaisedButton(
                onPressed: () {
                },
                color: Colors.white30,
                child: const Text('CONFIRM',
                    style: TextStyle(fontSize: 27, color: Colors.white)),
              ),
            ),
            if(fal && cou)
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
                    return new Etc();
                    }));
                  },
                  color: Colors.deepOrange,
                  child: const Text('CONFIRM',
                      style: TextStyle(fontSize: 27, color: Colors.white)),
                ),
              ),
          ],
        ),
      ),
    );
  }
}