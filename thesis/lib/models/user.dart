class User {
  static String studentId;
  String _name;
  String _student_id;
  String _password;
  String _flaglogged;


  setCurrID (String student_id){
    studentId = student_id;
  }

  User(this._name, this._student_id, this._password, this._flaglogged);

  User.map(dynamic obj) {
    this._name = obj['name'];
    this._student_id = obj['student_id'];
    this._password = obj['password'];
    this._flaglogged = obj['flag'];
  }

  String get name => _name;
  String get student_id => _student_id;
  String get password => _password;
  String get flaglogged => _flaglogged;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = _name;
    map["student_id"] = _student_id;
    map["password"] = _password;
    map["flaglogged"] = _flaglogged;
    return map;
  }
  @override
  String toString() {
    if(_flaglogged == 'logged') {
      studentId = _student_id;
      return "Login Complete..";
    }
    else {
      return studentId + " Login....";
    }
  }
}
