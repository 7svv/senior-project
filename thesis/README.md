# Studying Plan Assistance (SPA) - Senior Project

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Requirement:
- [Android Studio](https://developer.android.com/studio)
- [Flutter](https://flutter.dev/docs/get-started/install)

### First: 
- check flutter with command 
```flutterdoc
flutter doctor
```
- Install Flutter and Dart Plugin in Android Studio

### Second:
- When you open this project you have to set Flutter SDK

1. File->Settings->Language & Framework->Flutter
2. Choose flutter SDK path: the first time we install flutter, we choose the location where the flutter should be installed. Choose this location.
3. Click OK and the android studio will refresh. Carry on if the problem is solved.
4. If you are still stuck with the error. Goto this [link](https://flutter.io/get-started/install/) and install Dart.
5. Goto the same place in settings, ..Language & Framework->Dart and chose the SDK location.

- Pub get or get dependencies  above of android studio, it will show the command when you clone this repo first times or command in terminal like this.
```pub
flutter pub get | flutter pub upgrade
```


### Finally
> - Run file name "backmain.dart"