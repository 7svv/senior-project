import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:thesis/models/subjects.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:thesis/pages/main/allList.dart';

Future<List<Subjects>> fetchSubjects() async {
  var dbHelper = DatabaseHelper();
  Future<List<Subjects>> tempSubjects;
  tempSubjects = dbHelper.getSubjects();
  return tempSubjects;
}

class SubjectTable extends StatefulWidget {
  @override
  _SubjectTable createState() => _SubjectTable();
}

class _SubjectTable extends State<SubjectTable> {
  List<Subjects> listCredits = [];

  Future fetchSubjectsList() async {
    var dbHelper = DatabaseHelper();
    var result = await dbHelper.getSubjects();
    setState(() {
      listCredits = result;
    });
  }
  void filterSearchResults(String query) {

  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchSubjects();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Subject Table'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_circle_outline),
            tooltip: 'Add new Data',
            onPressed: () {
            },
          ),
        ],
      ),
      body: FutureBuilder<List<Subjects>>(
          future: fetchSubjects(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white
                      //border: Border.all(color: Colors.blueAccent)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        onChanged: (value) {
                        },
                        controller: null,
                        decoration: InputDecoration(
                            labelText: "Search",
                            hintText: "Search",
                            prefixIcon: Icon(Icons.search),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(25.0)))),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.white
                        //border: Border.all(color: Colors.blueAccent)
                    ),
                   child: Row(
                     children: <Widget>[
                       Expanded(child: Text('Subject ID', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18))),
                       Expanded(child: Text('Section', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18)),),
                       Expanded(child: Text('Semester', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18))),
                     ],
                   ),
                  ),
                  Expanded(
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return SingleChildScrollView(
                              child: Slidable(
                                actionPane: SlidableDrawerActionPane(),
                                actionExtentRatio: 0.25,
                                child: Container(
                                  color: Colors.white,
                                  child: ListTile(
                                    title: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(child: Text(snapshot.data[index].subject_id)),
                                        Expanded(child: Text(snapshot.data[index].section_id != null ? snapshot.data[index].section_id : "")),
                                        Expanded(child: Text(snapshot.data[index].semester.toString())),
                                      ],
                                    ),
                                  ),
                                ),
                                secondaryActions: <Widget>[
                                  IconSlideAction(
                                      caption: 'Edit',
                                      color: Colors.black45,
                                      icon: Icons.edit,
                                      onTap: () => null
                                  ),
                                  IconSlideAction(
                                    caption: 'Delete',
                                    color: Colors.red,
                                    icon: Icons.delete,
                                    onTap: () => null,
                                  ),
                                ],
                              ));
                        }),
                  ),
                ],
              );
            }
            else
            {
              return new Container(
                alignment: AlignmentDirectional.center,
                padding: EdgeInsets.all(20),
                child: new CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
