class Subjects {
  int id;
  String subject_id;
  String section_id;
  int semester;

  Subjects(this.id,this.subject_id, this.section_id, this.semester);

  Subjects.map(dynamic obj) {
    this.subject_id = obj['id'];
    this.subject_id = obj['subject_id'];
    this.section_id = obj['section_id'];
    this.semester = obj['semester'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["subject_id"] = subject_id;
    map["section_id"] = section_id;
    map["semester"] = semester;
    return map;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "$id "+ subject_id + " " + section_id + " $semester ";
  }
}
