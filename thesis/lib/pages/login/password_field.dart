import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:thesis/sizeconfig.dart';

class PasswordField extends StatefulWidget {
  final Function onSaved;
  final Function onChanged;

  PasswordField({Key key, @required this.onSaved, @required this.onChanged})
      : super(key: key);

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  String _password;
  bool _isPasswordDirty = false;

  // Initially password is obscure
  bool _obscureText = true;
  var vertical = SizeConfig.safeBlockVertical;
  var horizontal = SizeConfig.blockSizeHorizontal;
  var fontSize = SizeConfig.blockSizeVertical;
  String validatePassword(String value) {
    if (value.isEmpty) {
      return 'Password required.';
    }

    else if (value.length < 4)
      return 'Too short.';
    else
      return null;
  }

  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(horizontal*10,horizontal*3,horizontal*10,horizontal*3),
     child: new TextFormField(
      style: TextStyle(
        color: Color(0xff1372FF),
        fontSize: fontSize*2.5,
      ),
      keyboardType: TextInputType.text,
      obscureText: _obscureText,
      validator: validatePassword,
      autovalidate: _isPasswordDirty,
      onChanged: (value) {
        setState(() {
          _isPasswordDirty = true;
        });

        widget.onChanged(value);
      },
      onSaved: (value) {
        setState(() {
          _password = value;
        });

        widget.onSaved(value);
      },
      decoration: InputDecoration(
          helperText: '',
          labelText: 'Password',
          suffixIcon: new GestureDetector(
            onTap: () {
              setState(() {
                _obscureText = !_obscureText;
              });
            },
            child:
            new Icon(_obscureText ? Icons.visibility : Icons.visibility_off,
              size: fontSize*3.5,),
          ),
          prefixIcon: new Icon(Icons.lock,
              size: fontSize*3.5)
      ),
      )
    );

  }
}
