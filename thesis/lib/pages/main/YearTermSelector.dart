import 'package:flutter/material.dart';

String tempYearTerm = '1-1';
String getYearTerm(){
  return tempYearTerm;
}
setYearTerm(String value){
  switch(value) {
    case 'Year 1,Term 1':
      tempYearTerm = '1-1';
      break;
    case 'Year 1,Term 2':
      tempYearTerm = '1-2';
      break;
    case 'Year 2,Term 1':
      tempYearTerm = '2-1';
      break;
    case 'Year 2,Term 2':
      tempYearTerm = '2-2';
      break;
    case 'Year 3,Term 1':
      tempYearTerm = '3-1';
      break;
    case 'Year 3,Term 2':
      tempYearTerm = '3-2';
      break;
    case 'Year 4,Term 1':
      tempYearTerm = '4-1';
      break;
    case 'Year 4,Term 2':
      tempYearTerm = '4-2';
      break;
  }
}

// ignore: must_be_immutable
class YearTermSelector extends StatefulWidget {
  @override
  _YearTermSelectorState createState() {
    return _YearTermSelectorState();
  }
}
class _YearTermSelectorState extends State<YearTermSelector> {
  String dropdownValue = 'Year 1,Term 1';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: Icon(Icons.arrow_drop_down),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(
        fontSize: 18,
          color: Colors.black
      ),
      underline: Container(
        height: 2,
        color: Colors.grey[500]
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValue = newValue;
          setYearTerm(newValue);
          print(getYearTerm());
        });
      },
      items: <String>['Year 1,Term 1', 'Year 1,Term 2', 'Year 2,Term 1', 'Year 2,Term 2',
        'Year 3,Term 1', 'Year 3,Term 2', 'Year 4,Term 1', 'Year 4,Term 2']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      })
          .toList(),
    );
  }
}