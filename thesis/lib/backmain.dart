import 'package:flutter/material.dart';
import 'splashScreen.dart';

void main() => runApp(MainPage());

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext contextP) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: LoadingScreen(),
    );
  }
}
