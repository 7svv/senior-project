import 'package:flutter/material.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:thesis/models/user_timetable.dart';
List<User_timetable> _subjectCard = [];
List<User_timetable> _allSubject = [];
List<String> dropdownValue = [];
List<String> selectedSubjects = [];
String time = '';
int _term = 0;
String _day = 'mon';
String _studentId;

setUserMon(String studentId){
  _studentId = studentId;
}
setMonTerm(int term){
  _term = term;
}
setSubjectCard(List<User_timetable> tempTimetable){
  _subjectCard = tempTimetable;
}

List<String> setList(int index){
  List<String> tempList = [];
  String tempName = "";
  String tempTime = "";
  for(int i = 0; i < _subjectCard.length; i++){
    tempName = _subjectCard[i].subject_id + "\n" + _subjectCard[i].subject_name;
    tempTime = _subjectCard[i].time_start + "-\n" + _subjectCard[i].time_end;
    tempList.add(tempName);
    _allSubject.add(_subjectCard[i]);
    if(i == 0){
      time = tempTime;
    }
  }
  return tempList;
}
String getTime(String dropDown){
  String checkTime = dropDown.substring(0,5);
  for(int i = 0; i < _subjectCard.length; i++){
    if(_subjectCard[i].subject_id == checkTime){
      return _subjectCard[i].time_start + "-\n" + _subjectCard[i].time_end;
    }
  }
  return time;
}
List<User_timetable> getMonSubjectLists(){
  List<User_timetable> tempList = [];
  for(int i = 0; i < selectedSubjects.length; i++){
    for(int j = 0; j < _allSubject.length; j++){
      if(selectedSubjects[i] == _allSubject[j].subject_id){
        tempList.add(_allSubject[j]);
      }
    }
  }
  return tempList;
}

getSelectedSubjects(){
  print(selectedSubjects);
}

class MondayDropDown extends StatefulWidget {
  MondayDropDown({Key key}) : super(key: key);
  @override
  _DropDownList createState() => _DropDownList();
}

class _DropDownList extends State<MondayDropDown> {
//  List<String> x = ['TU110\nName Subject', 'TU120\nName Subject', 'TU130\nName Subject', 'TU101\nName Subject'];
  List<List<String>> dropDrownList  = [];
  List<User_timetable> allTimetable = [];
  List<User_timetable> term1Timetable = [];
  List<User_timetable> term2Timetable = [];
  List<User_timetable> yearTermTimetable = [];
  List<User_timetable> dayTimetable = [];
  List<List<User_timetable>> listDayTimetable = [];
  bool isLoading = false ;
  @override
  void initState() {
    super.initState();
    getAllTimetable(0);
    isLoading = true;
  }

  Future getAllTimetable(int semester) async {
//  Future<List<User_timetable>> getAllTimetable(int semester) async{
    print("getAllTimetable...");
    print(_studentId + " = student");
    var dbHelper = DatabaseHelper();
    List<User_timetable> allTempTimetable = [];
    List<User_timetable> tempTimetable1 = [];
    List<User_timetable> tempTimetable2 = [];
    Future<List<User_timetable>> tempTimetable;
    tempTimetable = dbHelper.getTimetable(_studentId,semester);
    tempTimetable.then(
          (itemList) {
        for(int i = 0; i < itemList.length; i++){
          allTempTimetable.add(itemList[i]);
          if(itemList[i].semester == 1){
            tempTimetable1.add(itemList[i]);
          }
          else if(itemList[i].semester == 2){
            tempTimetable2.add(itemList[i]);
          }
          else if(itemList[i].semester == 0){
            tempTimetable1.add(itemList[i]);
            tempTimetable2.add(itemList[i]);
          }
        }
      },
    );
    setState(() {
      allTimetable = allTempTimetable;
      term1Timetable = tempTimetable1;
      term2Timetable = tempTimetable2;
    });
  }
  List<User_timetable> getTimetableSemester(int semester){
    List<User_timetable> tempTimetable = [];
    for(int i = 0; i < allTimetable.length; i++){
      if(allTimetable[i].semester == semester){
        tempTimetable.add(allTimetable[i]);
      }
    }
    return tempTimetable;
  }
  List<User_timetable> getTimetableYearTerm(String yearTerm){
    print("yearTerm...");
    List<User_timetable> tempTimetable = [];
    for(int i = 0; i < allTimetable.length; i++){
      if(allTimetable[i].year_term == yearTerm && (allTimetable[i].semester == 0 || allTimetable[i].semester == 1)){
        tempTimetable.add(allTimetable[i]);
      }
    }
    return tempTimetable;
  }
  List<User_timetable> getTimetableInDay(int term, String day){
    print("day...");
    List<User_timetable> tempTimetable = [];
    if(term == 1) {
      for (int i = 0; i < term1Timetable.length; i++) {
        var tempSubject = term1Timetable[i].day;
        if(tempSubject.length <= 3){
          if (tempSubject == day) {
            tempTimetable.add(term1Timetable[i]);
          }
        }
        else{
          if (tempSubject.substring(0,3) == day || tempSubject.substring(4,7) == day) {
            tempTimetable.add(term1Timetable[i]);
          }
        }
      }
    }
    else{
      for (int i = 0; i < term2Timetable.length; i++) {
        var tempSubject = term2Timetable[i].day;
        if(tempSubject.length <= 3){
          if (tempSubject == day) {
            tempTimetable.add(term2Timetable[i]);
          }
        }
        else{
          if (tempSubject.substring(0,3) == day || tempSubject.substring(4,7) == day) {
            tempTimetable.add(term2Timetable[i]);
          }
        }
      }
    }
    return tempTimetable;
  }
  List<List<User_timetable>> getListTimetableList(List<User_timetable> timetable){
    print("List sec...");
    List<List<User_timetable>> tempTimetableList = [];
    for (int i = 0; i < timetable.length; i++) {
      List<User_timetable> tempTimetable = [];
      bool checkRepeat = false;
      String section = timetable[i].section_id.substring(0,4);
      if(i != 0){
        for(int j = 0; j < i; j++) {
          if (timetable[j].section_id.substring(0, 4) == section) {
            checkRepeat = true;
            break;
          }
        }
      }
      if(checkRepeat == false) {
        for (int k = i; k < timetable.length; k++) {
          if (timetable[k].section_id.substring(0,4) == section) {
            tempTimetable.add(timetable[k]);
          }
        }
        tempTimetableList.add(tempTimetable);
      }
    }
    return tempTimetableList;
  }

  @override
  Widget build(BuildContext context) {
    dropDrownList = [];
    _allSubject = [];
    print("Day: " + _day + " Term: " + _term.toString());
    dayTimetable = getTimetableInDay(_term, _day);
    listDayTimetable = getListTimetableList(dayTimetable);
    isLoading ? Center(child: CircularProgressIndicator()) : Container();
    print("LEN LIST: " + listDayTimetable.length.toString());

    if (listDayTimetable.length > 0) {
//      selectedSubjects = [];
      return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: listDayTimetable.length,
          itemBuilder: (context, index) {
//          print("Index: "+ index.toString() +" "+ listDayTimetable[index].toString());
            setSubjectCard(listDayTimetable[index]);
            if(dropdownValue.length != listDayTimetable.length){
              dropdownValue.add(listDayTimetable[index][0].subject_id + "\n" +
                  listDayTimetable[index][0].subject_name);
              selectedSubjects.add(listDayTimetable[index][0].subject_id);
            }
            dropDrownList.add(setList(index));
//          print("LIST: "+ listDayTimetable.toString());
            return Row(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                    padding: EdgeInsets.all(15.0),
                    child: //Text(choice.title, style: textStyle)
                    Text(time != null ? time : "???-\n???",
                        style: TextStyle(
                            fontSize: 20.0, color: Colors.blueAccent))),
                Expanded(
                    child: DropdownButton(
                      isExpanded: true,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue[index] =
                          newValue != null ? newValue : "???";
                          String tempSelected = newValue.substring(0,5);
                          selectedSubjects[index] = tempSelected != null ? tempSelected : "???";
                        });
                      },
                      value: dropdownValue[index] != null
                          ? dropdownValue[index]
                          : "???",
                      items: _dropDownItem(dropDrownList[index]),
                    )
                ),

//              Expanded(child: dropDown),
                Container(
                  padding: EdgeInsets.all(10.0),
                ),
              ],
            );
          }
      );
    }
    else {
      Future.delayed(new Duration(seconds: 1), () {
        isLoading = false;
      });
      return isLoading
          ? Container(
          alignment: AlignmentDirectional.center,
          padding: EdgeInsets.all(20),
          child: new CircularProgressIndicator())
          :
      Container(
          alignment: AlignmentDirectional.center,
          padding: EdgeInsets.all(20),
          child: new Text('ไม่พบข้อมูลรายวิชา',
              style: new TextStyle(fontSize: 20))
      );
    }
  }
  List<DropdownMenuItem<String>> _dropDownItem(List<String> list) {
    return list.map<DropdownMenuItem<String>>((
        String value) {
      return DropdownMenuItem<String>(
          value: value,
          child: Text(value, style: TextStyle(fontSize: 15.0),)
      );
    }).toList();
  }
}