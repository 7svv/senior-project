class Curriculums {
  String subject_id;
  String subject_name;
  int credit;
  String group_id;
  String year_term;
  String curriculum;
  String pair_subject;
  String pre_subject;

  Curriculums(
      this.subject_id,
      this.subject_name,
      this.credit,
      this.group_id,
      this.year_term,
      this.curriculum,
      this.pair_subject,
      this.pre_subject
      );

  Curriculums.map(dynamic obj) {
    this.subject_id = obj['subject_id'];
    this.subject_name = obj['subject_name'];
    this.credit = obj['credit'];
    this.group_id = obj['group_id'];
    this.year_term = obj['year_term'];
    this.curriculum = obj['curriculum'];
    this.pair_subject = obj['pair_subject'];
    this.pre_subject = obj['pre_subject'];

  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["subject_id"] = subject_id;
    map["subject_name"] = subject_name;
    map["credit"] = credit;
    map["group_id"] = group_id;
    map["year_term"] = year_term;
    map["curriculum"] = curriculum;
    map["pair_subject"] = pair_subject;
    map["pre_subject"] = pre_subject;
    return map;
  }

  @override
  String toString() {
    return subject_id + " " + subject_name + " "+ credit.toString() + " "+ group_id + " "+ year_term + " "
        + curriculum + " "+ pair_subject + " "+ pre_subject + " ";
  }
}