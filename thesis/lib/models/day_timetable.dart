import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:thesis/models/comfirmation.dart';
import 'package:thesis/models/current_credits.dart';
import 'package:thesis/models/user_timetable.dart';
import 'package:thesis/models/global_vars.dart' as globals;
import 'package:thesis/pages/timetable/timetabling.dart';

// ignore: must_be_immutable
class DayTimetable extends StatefulWidget {
  int indexList;
  String day;
  String yearTerm;
  List<User_timetable> planTimetable = [];
  List<User_timetable> termTimetable = [];
  List<User_timetable> dropdownValue = [];
  List<User_timetable> dayTimetable = [];
  List<User_timetable> repeatedSubject = [];
  List<List<User_timetable>> listDayTimetable = [];
  User_timetable emptySubject = new User_timetable(null, "None", "", 0, null,
      null, null, null, null, null, null, null, null, null, null);

  DayTimetable(this.planTimetable, this.termTimetable, this.day, this.yearTerm,
      this.repeatedSubject, this.indexList) {
    dayTimetable = getTimetableInDay(day);
    listDayTimetable = getListTimetableList(dayTimetable);
    dropdownValue = getDropdownVal(listDayTimetable);
    globals.setDropdownVal(indexList, day, dropdownValue);
  }

  List<User_timetable> getTimetableInDay(String day) {
//    print("getTimetableInDay...");
    List<User_timetable> tempTimetable = [];
    for (int i = 0; i < termTimetable.length; i++) {
      var tempSubject = termTimetable[i].day;
      if (day == "sun" &&
          termTimetable[i].year_term == yearTerm &&
          (termTimetable[i].section_id == "???" ||
              termTimetable[i].section_id == null ||
              (termTimetable[i].section_id != "???" &&
                  termTimetable[i].time_start == "???"))) {
        tempTimetable.add(termTimetable[i]);
      } else if (tempSubject.length <= 3) {
        if (tempSubject == day) {
          tempTimetable.add(termTimetable[i]);
        }
      } else {
        if (tempSubject.substring(0, 3) == day ||
            tempSubject.substring(4, 7) == day) {
          tempTimetable.add(termTimetable[i]);
        }
      }
    }

    return tempTimetable;
  }
  List<List<User_timetable>> getListTimetableList(
      List<User_timetable> timetable) {
//    print("getListTimetableList...");
    List<List<User_timetable>> tempTimetableList = [];
    for (int i = 0; i < timetable.length; i++) {
      List<User_timetable> tempTimetable = [];
      bool checkRepeat = false;
      if (timetable[i].section_id.length > 4) {
        String section = timetable[i].section_id.substring(0, 4);
        if (i != 0) {
          for (int j = 0; j < i; j++) {
            if (timetable[j].section_id.length > 4) {
              if (timetable[j].section_id.substring(0, 4) == section) {
                checkRepeat = true;
                break;
              }
            }
          }
        }
      }
      if (checkRepeat == false) {
        for (int k = i; k < timetable.length; k++) {
          if (timetable[k].section_id.length > 4) {
            String section = timetable[i].section_id.substring(0, 4);
            if (timetable[k].section_id.substring(0, 4) == section) {
              tempTimetable.add(timetable[k]);
              if(day == "sun"){
                tempTimetable.add(emptySubject);
              }
            }
          } else {
            if (day == "sun") {
              tempTimetable.add(timetable[k]);
              tempTimetable.add(emptySubject);
              break;
            }
          }
        }
        tempTimetableList.add(tempTimetable);
      }
    }
    if (day != "sun") {
      tempTimetableList = _setOverlayTime(tempTimetableList);
    }
    print(tempTimetableList);
    return tempTimetableList;
  }

  List<User_timetable> getDropdownVal(List<List<User_timetable>> tempList) {
    List<User_timetable> tempTimetable = [];
    for (int i = 0; i < tempList.length; i++) {
      tempTimetable
          .add(listDayTimetable[i][checkPlanSubject(listDayTimetable[i])]);
      repeatedSubject.add(listDayTimetable[i][checkPlanSubject(listDayTimetable[i])]);
    }
    print(tempTimetable);
    return tempTimetable;
  }
  int checkPlanSubject(List<User_timetable> tempList) {
    for (int i = 0; i < tempList.length; i++) {
      if (planTimetable.contains(tempList[i])) {
        if (repeatedSubject.contains( tempList[i] )) {
          var checkRepeat = repeatedSubject.firstWhere( (element) =>
          (element.subject_id == tempList[i].subject_id)
              && (element.section_id != tempList[i].section_id),
              orElse: () => null
          );
          if (checkRepeat != null) {
            continue;
          }
        }
        return i;
      }
    }
    return tempList.length-1;
  }
  List<List<User_timetable>> _setOverlayTime(List<List<User_timetable>> list) {
    List<List<User_timetable>> tempList = [];
    for (int i = 0; i < list.length; i++) {
      tempList.add(list[i]);
      var checkOverlay = list[i][0];
      for (int j = i + 1; j < tempList.length; j++) {
        if (_checkOverlayTime(checkOverlay, list[j][0])) {
          for (int k = 0; k < tempList[j].length; k++) {
            tempList[i].add(tempList[j][k]);
          }
          i++;
        }
      }
      tempList[i].add(emptySubject);
    }
    return tempList;
  }
  bool _checkOverlayTime(User_timetable temp1, User_timetable temp2) {
    var tempTimeStart1 = double.parse(temp1.time_start);
    var tempTimeStart2 = double.parse(temp2.time_start);
    var tempTimeEnd1 = double.parse(temp1.time_end);
    var tempTimeEnd2 = double.parse(temp2.time_end);
    if ((tempTimeEnd1 <= tempTimeStart2 || tempTimeEnd2 <= tempTimeStart1)) {
//      print("Time1: $tempTimeStart1|$tempTimeEnd1 - Time2: $tempTimeStart2|$tempTimeEnd2");
      return false;
    } else {
//    print("Time1: $tempTimeStart1|$tempTimeEnd1 - Time2: $tempTimeStart2|$tempTimeEnd2");
      return true;
    }
  }


  @override
  _DayTimetableState createState() => _DayTimetableState(
      key: UniqueKey(),
      planTimetable: planTimetable,
      termTimetable: termTimetable,
      dayTimetable: dayTimetable,
      listDayTimetable: listDayTimetable,
      dropdownValue: dropdownValue,
      repeatedSubject: repeatedSubject,
      day: day,
      yearTerm: yearTerm,
      indexList: indexList
  );
}

class _DayTimetableState extends State<DayTimetable> {

  User_timetable emptySubject = new User_timetable(null, "None", "", 0, null,
      null, null, null, null, null, null, null, null, null, null);
  int indexList;
  String day;
  String yearTerm;
  List<User_timetable> planTimetable = [];
  List<User_timetable> termTimetable = [];
  List<User_timetable> repeatedSubject = [];
  List<User_timetable> dropdownValue = [];
  List<List<User_timetable>> dropDrownList = [];
  List<User_timetable> dayTimetable = [];
  List<List<User_timetable>> listDayTimetable = [];
  bool isLoading = false;

  int indexDays(String day){
    switch(day){
      case "mon": return 0;
      case "tue": return 1;
      case "wed": return 2;
      case "thu": return 3;
      case "fri": return 4;
      case "sat": return 5;
      case "sun": return 6;
      default: return -1;
    }
  }
  _DayTimetableState({Key key,
    this.planTimetable,
    this.termTimetable,
    this.dayTimetable,
    this.listDayTimetable,
    this.dropdownValue,
    this.repeatedSubject,
    this.day,
    this.yearTerm,
    this.indexList
  });

  List<User_timetable> setList(List<User_timetable> tempListTimetable) {
    List<User_timetable> tempList = [];
    for (int i = 0; i < tempListTimetable.length; i++) {
      tempList.add(tempListTimetable[i]);
    }
    return tempList;
  }
  List<User_timetable> getDropdownVal(List<List<User_timetable>> tempList) {
    List<User_timetable> tempTimetable = [];
    for (int i = 0; i < tempList.length; i++) {
      tempTimetable
          .add(listDayTimetable[i][checkPlanSubject(listDayTimetable[i])]);
      repeatedSubject.add(listDayTimetable[i][checkPlanSubject(listDayTimetable[i])]);
    }
    return tempTimetable;
  }
  int checkPlanSubject(List<User_timetable> tempList) {
    for (int i = 0; i < tempList.length; i++) {
      if (planTimetable.contains(tempList[i])) {
        if (repeatedSubject.contains( tempList[i] )) {
          var checkRepeat = repeatedSubject.firstWhere( (element) =>
          (element.subject_id == tempList[i].subject_id)
              && (element.section_id != tempList[i].section_id),
              orElse: () => null
          );
          if (checkRepeat != null) {
            continue;
          }
        }
        return i;
      }
    }
    return tempList.length-1;
  }

  @override
  void initState() {
    super.initState();
    isLoading = true;
//    dropdownValue = globals.getDropdownVal(indexList, _day);
  }

  @override
  Widget build(BuildContext context) {
//    print(listDayTimetable);
//    print(planTimetable);
    dropDrownList = [];
    repeatedSubject = [];
    isLoading ? Center(child: CircularProgressIndicator()) : Container();
    CurrentCredits currCredits = CurrentCredits(UniqueKey(), globals.allCredits[indexList]);
    print("Day: " + day);
    print(listDayTimetable.length.toString() + " DropdownList");

    if (listDayTimetable.length > 0) {
      return Column(
        children: <Widget>[
          ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: listDayTimetable.length,
              itemBuilder: (context, index) {
                dropDrownList.add(setList(listDayTimetable[index]));
                var timeStart = globals.dayList[indexList][indexDays(day)].dropdownValue[index].time_start != null
                    ? globals.dayList[indexList][indexDays(day)].dropdownValue[index].time_start
                    : "???";
                var timeEnd = globals.dayList[indexList][indexDays(day)].dropdownValue[index].time_end != null
                    ? globals.dayList[indexList][indexDays(day)].dropdownValue[index].time_end
                    : "???";
                var time = timeStart + "-\n" + timeEnd;
                if (index != listDayTimetable.length - 1) {
                  return Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.all(15.0),
                            width: 100,
                            child: //Text(choice.title, style: textStyle)
                            Text(time != null ? time : "???-\n???",
                                style: TextStyle(
                                    fontSize: 20.0, color: Colors.blueAccent))),
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButton<User_timetable>(
                                isExpanded: true,
                                onChanged: (User_timetable newValue) {
                                  setState(() {
                                    globals.dayList[indexList][indexDays(day)].dropdownValue[index] =
                                    newValue != null ? newValue : emptySubject;
                                    globals.setAllCredits(indexList, day, dropdownValue);
                                    print(globals.dayList[indexList][indexDays(day)].dropdownValue);
                                    globals.setTimetableChanged(indexList);
                                    print(globals.allCredits[indexList]);
                                  });
                                },
                                value: globals.dayList[indexList][indexDays(day)].dropdownValue[index],
                                items: _dropDownItem(dropDrownList[index]),
                              )),
                        ),
                        Container(
                          padding: EdgeInsets.all(10.0),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.all(15.0),
                                width: 100,
                                child: //Text(choice.title, style: textStyle)
                                Text(time != null ? time : "???-\n???",
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.blueAccent))),
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10)),
                                  child: DropdownButton<User_timetable>(
                                    isExpanded: true,
                                    onChanged: (User_timetable newValue) {
                                      setState(() {
                                        globals.dayList[indexList][indexDays(day)].dropdownValue[index] =
                                        newValue != null ? newValue : emptySubject;
                                        globals.setAllCredits(indexList, day, dropdownValue);
                                        print(globals.dayList[indexList][indexDays(day)].dropdownValue[index]);
                                        currCredits = new CurrentCredits(UniqueKey(), globals.allCredits[indexList]);
                                        globals.setTimetableChanged(indexList);
                                        print(globals.allCredits[indexList]);
                                      });
                                    },
                                    value: globals.dayList[indexList][indexDays(day)].dropdownValue[index],
                                    items: _dropDownItem(dropDrownList[index]),
                                  )),
                            ),
                            Container(
                              padding: EdgeInsets.all(10.0),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                }
              }),
          currCredits,
          Confirmation(UniqueKey(), yearTerm, globals.termTimetable, globals.allTimetable, globals.termTimetable.allGroupCredits, globals.allCredits[indexList])
        ],
      );
    } else {
      Future.delayed(new Duration(seconds: 1), () {
        isLoading = false;
      });
      return isLoading
          ? Container(
          alignment: AlignmentDirectional.center,
          padding: EdgeInsets.all(20),
          child: new CircularProgressIndicator())
          : Container(
          alignment: AlignmentDirectional.center,
          padding: EdgeInsets.all(20),
          child: new Text('ไม่พบข้อมูลรายวิชา',
              style: new TextStyle(fontSize: 20)));
    }
  }

  List<DropdownMenuItem<User_timetable>> _dropDownItem(
      List<User_timetable> list) {
    return list.map<DropdownMenuItem<User_timetable>>((User_timetable value) {
      if (planTimetable.contains(value) && (value.year_term != null)) {
        return DropdownMenuItem<User_timetable>(
            value: value,
            child: Row(
              children: <Widget>[
                Container(
                    width: 100,
                    child: Text(value.subject_id + "\n" + value.subject_name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 15.0, color: Hexcolor('#065D00')))),
                Padding(
                  padding: EdgeInsets.all(10),
                ),
                Expanded(
                  child: IconButton(
                      icon: Icon(Icons.stars, color: Hexcolor('#065D00')),
                      onPressed: () {
                        showSimpleNotification(
                            Column(
                              children: <Widget>[
                                Icon(
                                  Icons.stars,
                                  color: Hexcolor('#065D00'),
                                  size: 30,
                                ),
                                Text(
                                    "This Subject is in the Curriculum Studying Plan.",
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        color: Hexcolor('#065D00')))
                              ],
                            ),
                            background: Colors.lightGreen);
                      }),
                ),
              ],
            ));
      } else {
        return DropdownMenuItem<User_timetable>(
            value: value,
            child: Text(
              value.subject_id + "\n" + value.subject_name,
              style: TextStyle(fontSize: 15.0),
            ));
      }
    }).toList();
  }
}
