class User_timetable {
  int id;
  int status;
  String subject_id;
  String subject_name;
  int credit;
  String group_id;
  String name_group;
  int all_credit;
  String year_term;
  String pair_subject;
  String pre_subject;
  String section_id;
  int semester;
  String day;
  String time_start;
  String time_end;

  User_timetable(this.status, this.subject_id, this.subject_name, this.credit,
      this.group_id, this.name_group, this.all_credit ,this.year_term, this.pair_subject, this.pre_subject, this.section_id,
      this.semester, this.day, this.time_start, this.time_end);

  setID(int id){
    this.id = id;
  }

  User_timetable.map(dynamic obj) {
    this.id = obj['id'];
    this.status = obj['status'];
    this.subject_id = obj['subject_id'];
    this.subject_name = obj['subject_name'];
    this.credit = obj['credit'];
    this.group_id = obj['group_id'];
    this.name_group = obj['name_group'];
    this.all_credit = obj['all_credit'];
    this.year_term = obj['year_term'];
    this.pair_subject = obj['pair_subject'];
    this.pre_subject = obj['pre_subject'];
    this.section_id = obj['section_id'];
    this.semester = obj['semester'];
    this.day = obj['day'];
    this.time_start = obj['time_start'];
    this.time_end = obj['time_end'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["status"] = status;
    map["subject_id"] = subject_id;
    map["subject_name"] = subject_name;
    map["credit"] = credit;
    map["group_id"] = group_id;
    map["name_group"] = group_id;
    map["all_credit"] = group_id;
    map["year_term"] = year_term;
    map["pair_subject"] = pair_subject;
    map["pre_subject"] = pre_subject;
    map["section_id"] = section_id;
    map["semester"] = semester;
    map["day"] = day;
    map["time_start"] = time_start;
    map["time_end"] = time_end;
    return map;
  }

  @override
  String toString() {
    return "Subject: " + subject_id != null ?  subject_id:"???" + " Sec: " + section_id != null ?  section_id:"???" + " Semester: " + semester.toString() != null ?  semester.toString():"???" + " Day: "+ day != null ?  day:"???" + " Time: " + time_start != null ?  time_start:"???" +"-"+time_end != null ?  time_end:"???";
  }
}
