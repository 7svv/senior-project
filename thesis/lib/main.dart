import 'package:flutter/material.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:thesis/pages/main/allList.dart';
import 'package:thesis/pages/login/login_page.dart';
import 'package:thesis/pages/login/register.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  final routes = {
    '/': (BuildContext context) => new LoginPage(),
  };

 @override
 Widget build(BuildContext context){
   DatabaseHelper().db;
   return new MaterialApp(
     title: 'SPA App',
     theme: new ThemeData(
         primarySwatch: Colors.blue,
//          fontFamily: 'THSarabunNew'
//         scaffoldBackgroundColor: Color(0xff1372FF)
     ),
     routes: routes,
   );
 }
}
