# Studying Plan Assistance (SPA) - Senior Project


## Requirement:
- [Android Studio](https://developer.android.com/studio)
- [Flutter](https://flutter.dev/docs/get-started/install)

### First: 
- check flutter with command 
```flutterdoc
flutter doctor
```
- Install Flutter and Dart Plugin in Android Studio

### Second:
- When you open this project you have to set Flutter SDK.

1. File->Settings->Language & Framework->Flutter
2. Choose flutter SDK path: the first time we install flutter, we choose the location where the flutter should be installed. Choose this location.
3. Click OK and the android studio will refresh. Carry on if the problem is solved.
4. If you are still stuck with the error. Goto this [link](https://flutter.io/get-started/install/) and install Dart.
5. Goto the same place in settings, ..Language & Framework->Dart and chose the SDK location.

- Pub get or get dependencies above of Android Studio, it will show the command when you clone this repo first times or go to **pubspec.yaml** file or command in terminal like this.
```pub
flutter pub get | flutter pub upgrade
```


### Finally
!! Make sure that you have the simulator || If not you can go to AVD Manager in Android Studio then create new Virtual Device then open it on.

Or you can use your phone to simulate in debug mode by following this step:
#####  Set up your Android device

To prepare to run and test your Flutter app on an Android device, you need an Android device running Android 4.1 (API level 16) or higher.

1. Enable Developer options and USB debugging on your device. Detailed instructions are available in the Android documentation.
2. Windows-only: Install the Google USB Driver.
3. Using a USB cable, plug your phone into your computer. If prompted on your device, authorize your computer to access your device.
4. In the terminal, run the flutter devices command to verify that Flutter recognizes your connected Android device. By default, Flutter uses the version of the Android SDK where your adb tool is based. If you want Flutter to use a different installation of the Android SDK, you must set the ANDROID_SDK_ROOT environment variable to that installation directory.

##### And then
> - Run file name "backmain.dart"
