import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CurrentCredits extends StatefulWidget {
  int allCredit = 0;
  Key key;
  CurrentCredits(this.key, this.allCredit);
  @override
  CurrentCreditsWidget createState() => CurrentCreditsWidget(key: key, allCredit: allCredit);
}

class CurrentCreditsWidget extends State {
  int allCredit = 0;
  CurrentCreditsWidget({Key key, this.allCredit});

  Widget build(BuildContext context) {
  print(allCredit);
    return Column(
      children: <Widget>[
        Text("Current Credits : $allCredit/22" ,
          style: TextStyle(fontSize: 15),),
      ],
    );
  }
}