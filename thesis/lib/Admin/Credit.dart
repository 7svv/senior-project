import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:thesis/models/credits.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

Future<List<Credits>> fetchCredits() async {
  var dbHelper = DatabaseHelper();
  Future<List<Credits>> tempCredits;
  tempCredits = dbHelper.getCredits();
  return tempCredits;
}

class Credit extends StatefulWidget {
  @override
  _CreditState createState() => _CreditState();
}

class _CreditState extends State<Credit> {
  List<Credits> listCredits = [];

  Future fetchCreditsList() async {
    var dbHelper = DatabaseHelper();
    var result = await dbHelper.getCredits();
    setState(() {
      listCredits = result;
    });
  }
  void filterSearchResults(String query) {

  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchCreditsList();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Credit Table'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_circle_outline),
            tooltip: 'Add new Data',
            onPressed: () {
            },
          ),
        ],
      ),
      body: FutureBuilder<List<Credits>>(
        future: fetchCredits(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
          return Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    color: Colors.white
                  //border: Border.all(color: Colors.blueAccent)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    onChanged: (value) {
                    },
                    controller: null,
                    decoration: InputDecoration(
                        labelText: "Search",
                        hintText: "Search",
                        prefixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(25.0)))),
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                  return SingleChildScrollView(
                      child: Slidable(
                        actionPane: SlidableDrawerActionPane(),
                        actionExtentRatio: 0.25,
                        child: Container(
                          color: Colors.white,
                          child: ListTile(
                                title: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(flex: 1,child: Text(snapshot.data[index].group_id)),
                                    Expanded(flex: 2, child: Text(snapshot.data[index].name_group)),
                                  ],
                                ),
                          ),
                        ),
                        secondaryActions: <Widget>[
                          IconSlideAction(
                              caption: 'Edit',
                              color: Colors.black45,
                              icon: Icons.edit,
                              onTap: () => null
                          ),
                          IconSlideAction(
                            caption: 'Delete',
                            color: Colors.red,
                            icon: Icons.delete,
                            onTap: () => null,
                          ),
                        ],
                      ));
                }),
              ),
            ],
          );
          }
          else
            {
              return new Container(
                  alignment: AlignmentDirectional.center,
                  padding: EdgeInsets.all(20),
                        child: new CircularProgressIndicator(),
              );
            }
//        child: SingleChildScrollView(
//          scrollDirection: Axis.horizontal,
//          child: DataTable(
//            columns: [
//              DataColumn(
//                label: Text('Group ID'),
//              ),
//              DataColumn(label: Text('Name Group')),
//              DataColumn(label: Text('Credit')),
//              DataColumn(label: Text('Curriculum')),
//            ],
//            rows: [
//              for(int i = 0; i < groupList.length; i++)
//                groupList[i]
//            ],
//          ),
//        ),
        }
        ),
    );
  }
}
