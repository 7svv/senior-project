import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:thesis/models/curriculums.dart';

Future<List<Curriculums>> fetchCurriculum() async {
  var dbHelper = DatabaseHelper();
  Future<List<Curriculums>> tempCurriculum;
  tempCurriculum = dbHelper.getCurriculums();
  return tempCurriculum;
}

class Curriculum extends StatefulWidget {
  @override
  _CurriculumsState createState() => _CurriculumsState();
}

class _CurriculumsState extends State<Curriculum> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Curriculum Table'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_circle_outline),
            tooltip: 'Add new Data',
            onPressed: () {
            },
          ),
        ],
      ),
      body: FutureBuilder<List<Curriculums>>(
          future: fetchCurriculum(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white
                      //border: Border.all(color: Colors.blueAccent)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        onChanged: (value) {

                        },
                        controller: null,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: "Search",
                            hintText: "Search",
                            prefixIcon: Icon(Icons.search),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(25.0)))),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: <Widget>[
                              SingleChildScrollView(
                                  child: Slidable(
                                    actionPane: SlidableDrawerActionPane(),
                                    actionExtentRatio: 0.25,
                                    child: Container(
                                      color: Colors.white,
                                      child: ListTile(
                                        title: Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Expanded(flex: 1,child: Text(snapshot.data[index].subject_id)),
                                            Expanded(flex: 2 ,child: Text(snapshot.data[index].subject_name)),
                                          ],
                                        ),
                                      ),
                                    ),
                                    secondaryActions: <Widget>[
                                      IconSlideAction(
                                          caption: 'Edit',
                                          color: Colors.black45,
                                          icon: Icons.edit,
                                          onTap: () => null
                                      ),
                                      IconSlideAction(
                                        caption: 'Delete',
                                        color: Colors.red,
                                        icon: Icons.delete,
                                        onTap: () => null,
                                      ),
                                    ],
                                  )
                              ),
                            ],
                          );
                        }),
                  ),
                ],
              );
            }
            else
            {
              return new Container(
                alignment: AlignmentDirectional.center,
                padding: EdgeInsets.all(20),
                child: new CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
