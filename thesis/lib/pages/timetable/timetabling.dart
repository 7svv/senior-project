import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:thesis/models/comfirmation.dart';
import 'package:thesis/models/credit_group.dart';
import 'package:thesis/models/timetable.dart';
import 'package:thesis/models/user_timetable.dart';
import 'package:thesis/pages/main/menuList.dart';
import 'package:thesis/pages/timetable/allTimetable.dart';
import 'package:thesis/models/current_credits.dart';
import 'package:thesis/pages/timetable/retire.dart';
import 'package:thesis/sizeconfig.dart';
import 'package:thesis/models/global_vars.dart' as globals;

String _studentId;

setUserTimetable(String studentId){
  _studentId = studentId;
}

// ignore: must_be_immutable
class Timetabling extends StatefulWidget {
  String currYearTerm;
  List<User_timetable> allTimetable = [];
  List<AllGroupCredits> allGroupCredits = [];
  Timetabling({Key key, this.allTimetable, this.allGroupCredits, this.currYearTerm});
  @override
  _TimetablingState createState() => _TimetablingState(allTimetable: allTimetable, allGroupCredits: allGroupCredits, currYearTerm: currYearTerm);
}

class _TimetablingState extends State<Timetabling> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey key = new GlobalKey();
  String currYearTerm;
  List<User_timetable> allTimetable = [];
  List<AllGroupCredits> allGroupCredits = [];
  List<Timetable> termTimetable = [];
  int _allCredits = 0;
  List<String> checkRepeat = [];
  List<String> yearTerm = [
    "1-1",
    "1-2",
    "2-1",
    "2-2",
    "3-1",
    "3-2",
    "4-1",
    "4-2"
  ];
  _TimetablingState({Key key, this.allTimetable, this.allGroupCredits, this.currYearTerm});
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(currYearTerm);
    int startYearTerm = yearTerm.indexOf(currYearTerm);
    globals.allCredits.add(0);
    globals.dayList.add([]);
    globals.allCurrentCredits.add(new CurrentCredits(UniqueKey(), 0));
    termTimetable.add(Timetable(key: UniqueKey(), allTimetable: allTimetable, yearTerm: yearTerm[startYearTerm+0], allGroupCredits: allGroupCredits, indexList: 0));
    termTimetable[0].setPassList(allTimetable);
    globals.setInitTimetable(termTimetable[0]);
  }

  int checkCurrentTerm(String currYearTerm){
    switch(currYearTerm){
      case "1-1": return 0; break;
      case "1-2": return 1; break;
      case "2-1": return 2; break;
      case "2-2": return 3; break;
      case "3-1": return 4; break;
      case "3-2": return 5; break;
      case "4-1": return 6; break;
      case "4-2": return 7; break;
      default: return -1; break;
    }
  }
  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    var vertical = SizeConfig.safeBlockVertical;
    var horizontal = SizeConfig.blockSizeHorizontal;
    var fontSize = SizeConfig.blockSizeVertical;
    var maxHeight = SizeConfig.screenHeight;
    double borSize = 20.0;
    addElective() {
      showModalBottomSheet(
          backgroundColor: Colors.transparent,
          //backgroundColor: Colors.pinkAccent,
          isScrollControlled: true,
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: (maxHeight-100),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(5)),
                  SizedBox(
                    height: 7.0,
                    width: 150,
                    child: new Center(
                      child: new Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(50.0),
                            ),
                            color: Hexcolor('#848489')
                        ),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(20)),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(35, 0, 35, 35),
                    child: Wrap(
                      children: <Widget>[
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Add Elective Subject',style: TextStyle(fontSize: 20),),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8,20,8,8),
                          child:TextField(
                            decoration: InputDecoration(
                              //prefixIcon: Icon(Icons.library_books),
                                disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                    borderSide: BorderSide(color: Colors.black)),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(borSize)),
                                    borderSide: BorderSide(color: Colors.black)),
                                labelText: "Enter ID Subject"
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:TextField(
                            decoration: InputDecoration(
                                disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                    borderSide: BorderSide(color: Colors.black)),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(borSize)),
                                    borderSide: BorderSide(color: Colors.black)),
                                labelText: "Enter Name Subject"
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:TextField(
                            decoration: InputDecoration(
                                disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                    borderSide: BorderSide(color: Colors.black)),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(borSize)),
                                    borderSide: BorderSide(color: Colors.black)),
                                labelText: "Enter Section"
                            ),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child:Column(
                              children: <Widget>[
                                Text('Choose Semester'),
                                TermSelector(),
                              ],
                            )
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CreditSelector()
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 30),
                          child: Center(
                            child: RaisedButton(
                              padding: EdgeInsets.fromLTRB(50,15,50,15),
                              child: Text("Submit", style: TextStyle(fontSize: 15)),
                              onPressed: (){
                              },
                              color: Colors.blue,
                              textColor: Colors.white,
                              //splashColor: Colors.grey,
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(50.0),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          });
    }

    return Scaffold(
            key: _scaffoldKey,
            extendBody: true,
            resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => MenuList()),
                  );
                },
              ),
              actions: <Widget>[
                IconButton(
                  icon: const Icon(Icons.calendar_today,size: 25,),
                  onPressed: () {
                    termTimetable[0].setOverall(globals.getAllDropdownVal(0));
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          Scaffold(
                            appBar: AppBar(
                              leading: IconButton(
                                icon: const Icon(Icons.arrow_back),
                                onPressed: () {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(builder: (context) => widget)
                                  );
                                },
                              ),
                              title: Text("Overall Timetable | Year: " +
                                  termTimetable[0].yearTerm.substring(0, 1) +
                                  " Term: " +
                                  termTimetable[0].yearTerm.substring(2, 3)
                            )),
                            body: termTimetable[0].overallTimetable,
                          )
                      ),
                    );
                  },
                ),
                IconButton(
                  icon: const Icon(Icons.repeat,size: 25,),
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => widget)
                    );
                  },
                ),
              ],
              title: Text("Year: " +
                  termTimetable[0].yearTerm.substring(0, 1) +
                  " Term: " +
                  termTimetable[0].yearTerm.substring(2, 3)
              ),
            ),
            body:termTimetable[0].timetabling,
            floatingActionButton: GestureDetector(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 100),
                child: FloatingActionButton.extended(
                    label: Icon(Icons.add),
                    elevation: 2,
                    backgroundColor: Colors.pinkAccent,
                    foregroundColor: Colors.white,
                    onPressed: () {
                      addElective();
                    }
                ),
              ),
            ),
      );
  }
}

class TermSelector extends StatefulWidget {
  @override
  TermSelectorWidget createState() => TermSelectorWidget();
}

class TermSelectorWidget extends State {
  int termId = 1;

  Widget build(BuildContext context) {

    return Row(
      children: <Widget>[
        Expanded(
          child: RadioListTile(
            groupValue: termId,
            title: Text('Term 1'),
            value: 1,
            onChanged: (val) {
              setState(() {
                termId = val;
                print("Select: $termId");
              });
            },
          ),
        ),

        Expanded(
          child: RadioListTile(
            groupValue: termId,
            title: Text('Term 2'),
            value: 2,
            onChanged: (val) {
              setState(() {
                termId = val;
                print("Select: $termId");
              });
            },
          ),
        ),
      ],
    );
  }
}

class CreditSelector extends StatefulWidget {
  @override
  CreditSelectorWidget createState() => CreditSelectorWidget();
}

class CreditSelectorWidget extends State {
  String _credit = '1';
  List<String> _credits = <String>['1', '2', '3', '4', '8'];
  double borSize = 20.0;
  Widget build(BuildContext context) {

    return FormField(
        builder:(FormFieldState state) {
          return InputDecorator(
            decoration: InputDecoration(
              disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(borSize)),
                  borderSide: BorderSide(color: Colors.black)),
              labelText: 'Credit',
            ),
            isEmpty: _credit == '',
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                value: _credit,
                isDense: true,
                onChanged: (String newValue) {
                  setState(() {
                    _credit = newValue;
                    print(_credit);
                  });
                },
                items: _credits.map((String value) {
                  return new DropdownMenuItem(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
              ),
            ),
          );
        }
    );
  }
}