import 'package:flutter/material.dart';
import 'package:thesis/pages/login/login_page.dart';
import 'home.dart';

class Etc extends StatelessWidget {

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: true,
            //`true` if you want Flutter to automatically add Back Button when needed,
            //or `false` if you want to force your own back button every where
            title: Text('Maintenance'),
            leading: IconButton(icon:Icon(Icons.arrow_back),
              onPressed:() {
                Navigator.of(context)
                    .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
                return new LoginPage();
                }));
              },
            )
        ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/bg.png'),
          fit: BoxFit.cover),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Maintenance ...', style: TextStyle(fontSize: 40)),
              SizedBox(
                height: 50,
              ),
            ]
    ))));
  }
}