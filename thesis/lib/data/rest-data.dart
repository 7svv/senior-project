import 'dart:async';
import 'package:thesis/models/user.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:password/password.dart';

class RestData {

  Future<User> login( String student_id, password) async {
    print("login " + student_id);
    String flagLogged = "logged";
    var user = new User(null, student_id, password, null);
    var db = new DatabaseHelper();
    var userRetorno = new User(null,null,null,null);
    userRetorno = await db.getUser(student_id);
    if(userRetorno != null && Password.verify(password, userRetorno.password)){
      flagLogged = "logged";
      return new Future.value(new User(null, student_id, password, flagLogged));
    }else {
      flagLogged = "not";
      return new Future.value(new User(null, student_id, password, flagLogged));
    }
  }
}
