import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thesis/Admin/Credit.dart';
import 'package:thesis/Admin/Curriculum.dart';
import 'package:thesis/Admin/SubjectTable.dart';

void main() => runApp(new MaterialApp(home: AdminDB()));

class AdminDB extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
            appBar: AppBar(
              elevation: 2.0,
              backgroundColor: Colors.white,
              title: Text('Dashboard',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontSize: 30.0)),
              actions: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('Admin',
                          style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.w700,
                              fontSize: 14.0)),
                      Icon(Icons.arrow_drop_down, color: Colors.black54)
                    ],
                  ),
                )
              ],
            ),
            body: Container(
              child: Column(children: <Widget>[
                //Credit
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Credit()),
                    );
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 10,
                    margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(
                              Icons.assignment,
                              size: 60,
                              color: Colors.pink,
                            ),
                            title: Text('Credit'),
                            subtitle: Text('Credits for each subject'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                //Curriculum
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Curriculum()),
                    );
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 10,
                    margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(
                              Icons.chrome_reader_mode,
                              size: 60,
                              color: Colors.blueAccent,
                            ),
                            title: Text('Curriculum'),
                            subtitle: Text('All subjects in the curriculum'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                //Subject
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SubjectTable()),
                    );
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 10,
                    margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(
                              Icons.class_,
                              size: 60,
                              color: Colors.purple,
                            ),
                            title: Text('Subject'),
                            //subtitle: Text('Courses that can be freely studied'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                //Section
                GestureDetector(
                  onTap: () {
                    print("Section");
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 10,
                    margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(
                              Icons.schedule,
                              size: 60,
                              color: Colors.lightGreen,
                            ),
                            title: Text('Section'),
                            subtitle: Text('Lessons for each subject'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                //Elective Subject
                GestureDetector(
                  onTap: () {
                    print("Elective Subject");
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 10,
                    margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(
                              Icons.school,
                              size: 60,
                              color: Colors.amber,
                            ),
                            title: Text('Elective Subject'),
                            subtitle: Text('Courses that can be freely studied'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ]),
            ));
  }
}
