import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thesis/models/credit_group.dart';
import 'package:thesis/models/day_timetable.dart';
import 'package:thesis/models/overall_timetable-table.dart';
import 'package:thesis/models/user_timetable.dart';
import 'package:thesis/models/current_credits.dart';
import 'package:thesis/models/overall_timetable.dart';
import 'package:thesis/pages/timetable/timetabling.dart';
import 'package:thesis/sizeconfig.dart';
import 'package:thesis/models/global_vars.dart' as globals;

class Timetable {

  int indexList;
  List<User_timetable> allTimetable = [];
  List<User_timetable> planTimetable = [];
  List<User_timetable> termTimetable = [];
  List<AllGroupCredits> allGroupCredits;
  List<User_timetable> passSubjects = [];
  List<DayTimetable> dayList = [];
  List<String> checkRepeat = [];
  User_timetable internSubject;
  List<User_timetable> thesisSubjects = [];
  List<User_timetable> coEducationSubjects =[];
  String yearTerm;
  int _term;
  int allCredits;
  TabBarTimetable timetabling;
  OverallTimetable overallTimetable;
  OverallTimetableTable overallTimetableTable;
  CurrentCredits currentCredits;
  ShowOverall showOverall;
  bool checkFinalPass = false;
  Timetable({Key key, this.allTimetable, this.yearTerm, this.allGroupCredits, this.indexList}){
    globals.allTimetable = this.allTimetable;
    _term = int.parse(this.yearTerm.substring(2,3));
    internSubject = allTimetable.firstWhere((element) => element.year_term == "3-3", orElse: () => null);
    var thesisSubject1 = allTimetable.firstWhere((element) => element.subject_id == "CN404", orElse: () => null);
    var thesisSubject2 = allTimetable.firstWhere((element) => element.subject_id == "CN405", orElse: () => null);
    thesisSubjects.add(thesisSubject1);
    thesisSubjects.add(thesisSubject2);
    var coSubject1 = allTimetable.firstWhere((element) => element.subject_id == "CN406", orElse: () => null);
    var coSubject2 = allTimetable.firstWhere((element) => element.subject_id == "CN407", orElse: () => null);
    coEducationSubjects.add(coSubject1);
    coEducationSubjects.add(coSubject2);
  }
  List<User_timetable> getTimetableYearTerm(String yearTerm) {
//    print("getTimetableYearTerm...");
    List<User_timetable> tempTimetable = [];
    List<String> checkRepeat = [];
    for (int i = 0; i < allTimetable.length; i++) {
      if (allTimetable[i].year_term == yearTerm && (allTimetable[i].semester == _term || allTimetable[i].semester == 0 || allTimetable[i].semester == 3)) {
        if(!checkRepeat.contains(allTimetable[i].subject_id)){
          tempTimetable.add(allTimetable[i]);
          checkRepeat.add(allTimetable[i].subject_id);
        }
      }
    }
    return tempTimetable;
  }
  List<User_timetable> getB4TimetableYearTerm(String yearTerm) {
//    print("getTimetableYearTerm...");
    List<User_timetable> tempTimetable = [];
    List<String> checkRepeat = [];
    for (int i = 0; i < allTimetable.length; i++) {
      if (allTimetable[i].year_term == yearTerm) {
        if(!checkRepeat.contains(allTimetable[i].subject_id)){
          tempTimetable.add(allTimetable[i]);
          checkRepeat.add(allTimetable[i].subject_id);
        }
      }
    }
    return tempTimetable;
  }
  List<User_timetable> getTimetableSemester(int term){
//    print("getTimetableSemester...");
    List<User_timetable> tempTimetable = [];
    for(int i = 0; i < allTimetable.length; i++){
      if(allTimetable[i].semester == term || allTimetable[i].semester == 0 || allTimetable[i].semester == 3){
        tempTimetable.add(allTimetable[i]);
      }
    }
    return tempTimetable;
  }

  setPassList(List<User_timetable> tempPass) {
//    print("setPassSubject...");
    dayList = [];
    passSubjects = [];
    List<String> passCheck = [];
    List<User_timetable> checkTemp = [];
    bool checkPlanPass = true;
    planTimetable = getTimetableYearTerm(yearTerm);
    print("first plan : " + planTimetable.toString());
    var b4planTimetable = getTimetableYearTerm(yearTerm);
    termTimetable = getTimetableSemester(_term);

    for(int i = 0; i < tempPass.length; i++){
      if(tempPass[i].status == 1){
        passSubjects.add(tempPass[i]);
      }
    }
    for(int i = 0; i < passSubjects.length; i++){
      if(!passCheck.contains(passSubjects[i].subject_id)){
        passCheck.add(passSubjects[i].subject_id);
      }
    }

    if(int.parse(yearTerm.substring(0,1)) > 3 && !passCheck.contains(internSubject.subject_id)){
      passSubjects.add(internSubject);
    }
    planTimetable.removeWhere((element) => passCheck.contains(element.subject_id));
    print("pass : " + passSubjects.toString());
    print("b4 term : " + termTimetable.toString());
    _setTermList();
    _checkPreSubject(planTimetable);
    _checkPairSubject(planTimetable);
    _checkPreSubject(termTimetable);
    _checkPairSubject(termTimetable);

    checkTemp = []..addAll(termTimetable);
    checkPlanPass = _checkB4Plan(passCheck);
    print(checkPlanPass);
    if(!checkPlanPass){
      for(int i = 0; i < passCheck.length; i++) {
        checkTemp.removeWhere( (element) =>
            passCheck.contains(element.subject_id) );
      }
      print("start group : " + getAllGroupCredits().toString());
      planTimetable = _setPlanTimetable(planTimetable, checkTemp);
    }
    if(passCheck.contains(thesisSubjects[0].subject_id)){
      planTimetable.removeWhere((element) => coEducationSubjects.contains(element));
      if(!passCheck.contains(coEducationSubjects[0].subject_id)){
        passSubjects.add(coEducationSubjects[0]);
      }
      if(!passCheck.contains(coEducationSubjects[1].subject_id)){
        passSubjects.add(coEducationSubjects[1]);
      }
      var tempGroup = allGroupCredits.firstWhere((element) => element.group_id == "2.3.2.2", orElse: () => null);
      if(tempGroup != null){
        tempGroup.all_credit -= 3;
      }
    }
    if(passCheck.contains(coEducationSubjects[0].subject_id)){
      planTimetable.removeWhere((element) => thesisSubjects.contains(element));
      if(!passCheck.contains(thesisSubjects[0].subject_id)){
        passSubjects.add(thesisSubjects[0]);
      }
      if(!passCheck.contains(thesisSubjects[1].subject_id)){
        passSubjects.add(thesisSubjects[1]);
      }
      var tempGroup = allGroupCredits.firstWhere((element) => element.group_id == "2.3.2.2", orElse: () => null);
      if(tempGroup != null){
        tempGroup.all_credit -= 9;
      }
    }
    _checkXXXXX(planTimetable, checkTemp);
    print("plan in check: " + planTimetable.toString());
    print("term in check: " + termTimetable.toString());

    dayList.add(new DayTimetable(planTimetable, termTimetable, "mon", yearTerm, [], indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "tue", yearTerm, dayList[0].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "wed", yearTerm, dayList[1].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "thu", yearTerm, dayList[2].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "fri", yearTerm, dayList[3].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "sat", yearTerm, dayList[4].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "sun", yearTerm, dayList[5].repeatedSubject, indexList));
    globals.dayList[indexList] = []..addAll(dayList);

    print("final pass : " + passSubjects.toString());
    print("final plan : " + planTimetable.toString());

    allGroupCredits = getAllGroupCredits();
    globals.setGroupCredits(getAllGroupCredits());
    timetabling = new TabBarTimetable(indexList, overallTimetable, currentCredits);
    globals.getAllCredits(indexList);
    allCredits = getCredits(planTimetable);
  }

  setPassSubjects(List<User_timetable> tempPlanSubjects, List<User_timetable> tempPassSubjects) {
//    print("setPassSubject...");
    dayList = [];
    List<String> passCheck = [];
    List<User_timetable> checkTemp = [];
    bool checkPlanPass = true;
    planTimetable = getTimetableYearTerm(yearTerm);
    print("first plan : " + planTimetable.toString());
    termTimetable = getTimetableSemester(_term);
    passSubjects = []..addAll(tempPassSubjects);
    for(int i = 0; i < passSubjects.length; i++){
      if(!passCheck.contains(passSubjects[i].subject_id)){
        passCheck.add(passSubjects[i].subject_id);
      }
    }

    if (int.parse(yearTerm.substring(0, 1)) > 3 && !passCheck.contains(internSubject.subject_id)) {
      passSubjects.add(internSubject);
    }
    planTimetable.removeWhere((element) => passCheck.contains(element.subject_id));

    _setTermList();
    _checkPreSubject(planTimetable);
    _checkPairSubject(planTimetable);
    _checkPreSubject(termTimetable);
    _checkPairSubject(termTimetable);

    termTimetable.sort((a, b) {
      var checkB = b.year_term != null ? b.year_term : "9";
      var checkA = a.year_term != null ? a.year_term : "9";
      return checkA.compareTo(checkB);
    });

    checkTemp = []..addAll(termTimetable);
    checkPlanPass = _checkB4Plan(passCheck);
    print(checkPlanPass);
    if(!checkPlanPass){
      for(int i = 0; i < passCheck.length; i++) {
        checkTemp.removeWhere( (element) =>
        passCheck.contains(element.subject_id) );
      }
      planTimetable = _setPlanTimetable(planTimetable, checkTemp);
    }

    _checkXXXXX(planTimetable, checkTemp);
    dayList.add(new DayTimetable(planTimetable, termTimetable, "mon", yearTerm, [], indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "tue", yearTerm, dayList[0].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "wed", yearTerm, dayList[1].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "thu", yearTerm, dayList[2].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "fri", yearTerm, dayList[3].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "sat", yearTerm, dayList[4].repeatedSubject, indexList));
    dayList.add(new DayTimetable(planTimetable, termTimetable, "sun", yearTerm, dayList[5].repeatedSubject, indexList));
    globals.dayList[indexList] = []..addAll(dayList);

    timetabling = new TabBarTimetable(indexList, overallTimetable, currentCredits);
    for (int i = 0; i < planTimetable.length; i++) {
      if (!passSubjects.contains(planTimetable[i])) {
        passSubjects.add(planTimetable[i]);
      }
      if(planTimetable[i].subject_id == "TU120"){
        var tempSub = allTimetable.firstWhere((element) => element.subject_id == "TU109", orElse: () => null);
        if(tempSub != null){
          passSubjects.add(tempSub);
        }
      }
      else if(planTimetable[i].subject_id == "TU109"){
        var tempSub = allTimetable.firstWhere((element) => element.subject_id == "TU120", orElse: () => null);
        if(tempSub != null){
          passSubjects.add(tempSub);
        }
      }
    }
    for(int i = 0; i < planTimetable.length; i++){
      List<User_timetable> checkRepeat = [];
      if(checkRepeat.contains(planTimetable[i])){
        planTimetable.removeAt(i);
        i--;
        continue;
      }
      if(thesisSubjects.contains(planTimetable[i])){
        planTimetable.removeWhere((element) => coEducationSubjects.contains(element));
        if(!passCheck.contains(coEducationSubjects[0].subject_id)){
          passSubjects.add(coEducationSubjects[0]);
        }
        if(!passCheck.contains(coEducationSubjects[1].subject_id)){
          passSubjects.add(coEducationSubjects[1]);
        }
        var tempGroup = allGroupCredits.firstWhere((element) => element.group_id == "2.3.2.2", orElse: () => null);
        if(tempGroup != null){
          tempGroup.all_credit -= 3;
        }
        break;
      }
      else if(coEducationSubjects.contains(planTimetable[i])){
        planTimetable.removeWhere((element) => thesisSubjects.contains(element));
        if(!passCheck.contains(thesisSubjects[0].subject_id)){
          passSubjects.add(thesisSubjects[0]);
        }
        if(!passCheck.contains(thesisSubjects[1].subject_id)){
          passSubjects.add(thesisSubjects[1]);
        }
        var tempGroup = allGroupCredits.firstWhere((element) => element.group_id == "2.3.2.2", orElse: () => null);
        if(tempGroup != null){
          tempGroup.all_credit -= 9;
        }
        break;
      }
    }
    print("final pass : " + passSubjects.toString());
    allGroupCredits = getAllGroupCredits();
    checkFinalPass = checkAllGroupPass(allGroupCredits);
    globals.getAllCredits(indexList);
    allCredits = getCredits(planTimetable);
  }

  _setPlanTimetable(List<User_timetable> tempPlan, List<User_timetable> tempTerm){
    List terms = [
      "1-1",
      "1-2",
      "2-1",
      "2-2",
      "3-1",
      "3-2",
      "4-1",
      "4-2"
    ];

    int currTerm = terms.indexOf(yearTerm);
    tempPlan = [];

    tempTerm.sort((a, b) {
      var checkB = b.year_term != null ? b.year_term : "9";
      var checkA = a.year_term != null ? a.year_term : "9";
      return checkA.compareTo(checkB);
    });
    for(int i = 0; i < tempTerm.length; i++){
      if(tempTerm[i].subject_id == "CN403"){
        continue;
      }
      if(thesisSubjects.contains(tempTerm[i].subject_id) || coEducationSubjects.contains(tempTerm[i].subject_id)){
        int checkCNPass = 0;
        for(int m = 0; m < passSubjects.length; m++){
          if(passSubjects[m].subject_id.substring(0,2) == "CN"){
            checkCNPass += passSubjects[m].credit;
          }
        }
        if(checkCNPass < 22){
          continue;
        }
      }
      List<AllGroupCredits> tempCheckCredits = getCheckAllGroupCredits(tempPlan);
      var checkCredits = getCredits(tempPlan);
//      print("check Sub : "+ tempTerm[i].subject_id);
      if(!checkAllGroupMaxCredits(tempCheckCredits)) {
        if (_checkSubjectAdd( tempTerm[i], checkCredits, tempPlan )) {
//          print( "pass Sub : " + tempTerm[i].subject_id );
          var checkCreditGroup = tempCheckCredits.firstWhere( (
                element) => element.group_id == tempTerm[i].group_id,
                orElse: () => null );
            if (checkCreditGroup != null &&
                (checkCreditGroup.currCredits >= checkCreditGroup.all_credit)) {
//              print(checkCreditGroup.group_id+ " : " + checkCreditGroup.currCredits.toString());
              continue;
            }
            if (tempTerm[i].year_term != null && (currTerm + 1 <= 7) &&
                (tempTerm[i].year_term == terms[currTerm + 1])) {
              break;
            }
            print( "Add : " + tempTerm[i].toString( ) );
            tempPlan.add( tempTerm[i] );
            if(tempTerm[i].subject_id.substring(0,5) != "XXXXX"){
              checkCreditGroup.currCredits += tempTerm[i].credit;
            }
        }
      }
    }
    return tempPlan;
  }
  int getCredits(List<User_timetable> tempList){
    int sum = 0;
    for(int i = 0; i < tempList.length; i++){
      sum += (tempList[i].credit != null ? tempList[i].credit:0);
    }
    return sum;
  }
  bool checkAllGroupMaxCredits(List<AllGroupCredits> tempGroup){
    for(int i = 0; i < tempGroup.length; i++){
      if(tempGroup[i].currCredits < tempGroup[i].all_credit){
        return false;
      }
    }
    return true;
  }
  bool _checkSubjectAdd(User_timetable tempSub, int currCredits, List<User_timetable> tempPlan){
    bool checkProcess = true;
    if((tempSub.credit + currCredits) > 22){
//      print("> 22");
      return false;
    }
    if(tempSub.pre_subject != null){
//      print("pre check.");
      var checkPass = passSubjects.firstWhere((element) => element.subject_id == tempSub.pre_subject, orElse:() => null );
      if(!passSubjects.contains(checkPass) || checkPass == null){
        return false;
      }
    }
    for(int i = 0; i < tempPlan.length; i++){
      if(tempPlan[i].subject_id == tempSub.subject_id){
        return false;
      }
      if(tempSub.section_id.length > 4 && tempSub.time_start != "???" &&
          tempPlan[i].section_id.length > 4 && tempPlan[i].time_start != "???") {
        if (_checkOverlayTime( tempPlan[i], tempSub )) {
//          print("tempPlan[$i] " + tempPlan[i].subject_id);
//          print("Overlay.");
          return false;
        }
      }
    }

    return checkProcess;
  }
  bool _checkB4Plan(List<String> passList){
    print("cur year: " + yearTerm);
    List<User_timetable> b4Term = [];
    List terms = [
      "1-1",
      "1-2",
      "2-1",
      "2-2",
      "3-1",
      "3-2",
      "4-1",
      "4-2"
    ];
    int currTerm = terms.indexOf(yearTerm);
    if(currTerm < 0){
      return false;
    }
    for(int i = 0; i < terms.length; i++){
      var tempList = getB4TimetableYearTerm(terms[i]);
      List<User_timetable> tempTerm = [];
       for(int j = 0; j < tempList.length; j++){
         tempTerm.add(tempList[j]);
       }
       for(int k = 0; k < tempTerm.length; k++){
         if(!passList.contains(tempTerm[k].subject_id)){
           return false;
         }
       }
      b4Term.addAll(tempTerm);
      if(terms[i] == terms[currTerm-1]){
        break;
      }
    }

    for(int i = 0; i < b4Term.length; i++){
      if(!passList.contains(b4Term[i].subject_id)){
        return false;
      }
    }
    return true;
  }
  bool _checkOverlayTime(User_timetable temp1, User_timetable temp2) {
    var tempTimeStart1 = double.parse(temp1.time_start);
    var tempTimeStart2 = double.parse(temp2.time_start);
    var tempTimeEnd1 = double.parse(temp1.time_end);
    var tempTimeEnd2 = double.parse(temp2.time_end);
    if ((tempTimeEnd1 <= tempTimeStart2 || tempTimeEnd2 <= tempTimeStart1) ||
        (temp1.day != temp2.day)) {
//      print("Time1: $tempTimeStart1|$tempTimeEnd1 - Time2: $tempTimeStart2|$tempTimeEnd2");
      return false;
    } else {
//    print("Time1: $tempTimeStart1|$tempTimeEnd1 - Time2: $tempTimeStart2|$tempTimeEnd2");
      return true;
    }
  }
  setOverall(List<User_timetable> tempList){
    overallTimetableTable = new OverallTimetableTable(tempList);
    overallTimetable = new OverallTimetable(
        dayList[0].dropdownValue, dayList[1].dropdownValue, dayList[2].dropdownValue,
        dayList[3].dropdownValue, dayList[4].dropdownValue, dayList[5].dropdownValue, dayList[6].dropdownValue);
    Timetabling newTimetabling = new Timetabling(key: UniqueKey(), allTimetable: allTimetable, allGroupCredits: allGroupCredits, currYearTerm: yearTerm);
    showOverall = new ShowOverall(overallTimetable, newTimetabling);
  }
  _setTermList(){
    for(int i = 0; i < passSubjects.length; i++){
        if(termTimetable.contains(passSubjects[i])){
          termTimetable.removeAt(termTimetable.indexOf(passSubjects[i]));
          i--;
        }
    }
    for(int i = 0; i < allGroupCredits.length; i++){
      if(allGroupCredits[i].all_credit != null && allGroupCredits[i].currCredits >= allGroupCredits[i].all_credit){
        if(allGroupCredits[i].group_id == "2.3.1") {
          var tempCheck = termTimetable.indexWhere( (element) =>
          element.group_id == allGroupCredits[i].group_id &&
              element.year_term != null );
          if (tempCheck >= 0) {
            passSubjects.add(
                termTimetable[termTimetable.indexWhere( (element) => element
                    .group_id == allGroupCredits[i].group_id &&
                    element.year_term != null )] );
          }
        }
        else if(allGroupCredits[i].group_id == "1.5.2"){
          var tempCheck = termTimetable.indexWhere( (element) =>
          element.group_id == allGroupCredits[i].group_id &&
              element.year_term != null );
          if (tempCheck >= 0) {
            passSubjects.add(
                termTimetable[termTimetable.indexWhere( (element) => element
                    .group_id == allGroupCredits[i].group_id &&
                    element.year_term != null )] );
          }        }
        else if(allGroupCredits[i].group_id == "2.3.2.2"){
          var tempCheck = termTimetable.indexWhere( (element) =>
          element.group_id == allGroupCredits[i].group_id &&
              element.year_term != null );
          if (tempCheck >= 0) {
            passSubjects.add(
                termTimetable[termTimetable.indexWhere( (element) => element
                    .group_id == allGroupCredits[i].group_id &&
                    element.year_term != null )] );
          }        }
        termTimetable.removeWhere((element) => element.group_id == allGroupCredits[i].group_id);
      }
    }
  }
  List<AllGroupCredits> getAllGroupCredits(){
    List<String> checkRepeat = [];
    List<AllGroupCredits> tempAllGroupCredits = allGroupCredits.map((element)=>AllGroupCredits(element.group_id, element.name_group, element.all_credit, element.curriculum)).toList();
    for(int i = 0; i < passSubjects.length; i++){
      var checkGroup = tempAllGroupCredits.firstWhere((element) => element.group_id == passSubjects[i].group_id, orElse:() => null);
      if(checkGroup != null && !checkRepeat.contains(passSubjects[i].subject_id)) {
        checkGroup.currCredits += passSubjects[i].credit;
        checkRepeat.add(passSubjects[i].subject_id);
      }
      else{
        continue;
      }
    }
    return tempAllGroupCredits;
  }
  List<AllGroupCredits> getCheckAllGroupCredits(List<User_timetable> tempList){
    List<String> checkRepeat = [];
    List<AllGroupCredits> tempAllGroupCredits = allGroupCredits.map((element)=>AllGroupCredits(element.group_id, element.name_group, element.all_credit, element.curriculum)).toList();
    for(int i = 0; i < passSubjects.length; i++){
      var checkGroup = tempAllGroupCredits.firstWhere((element) => element.group_id == passSubjects[i].group_id, orElse:() => null);
      if(checkGroup != null && !checkRepeat.contains(passSubjects[i].subject_id)) {
        checkGroup.currCredits += passSubjects[i].credit;
        checkRepeat.add(passSubjects[i].subject_id);
      }
      else{
        continue;
      }
    }
    for(int i = 0; i < tempList.length; i++){
      var checkGroup = tempAllGroupCredits.firstWhere((element) => element.group_id == tempList[i].group_id, orElse:() => null);
      if(checkGroup != null) {
        checkGroup.currCredits += tempList[i].credit;
      }
      else{
        print(tempList[i].toString() + " " + tempList[i].group_id);
        continue;
      }
    }
    return tempAllGroupCredits;
  }
  List<User_timetable> getPassSubjects() {
    return passSubjects;
  }
  _checkPreSubject(List<User_timetable> tempList){
//    print("1 " + tempList.toString() + " " + tempList.length.toString());
    for(int i = 0; i < tempList.length; i++){
      if(tempList[i].pre_subject != null){
        var checkPass = passSubjects.firstWhere((element) => element.subject_id == tempList[i].pre_subject, orElse:() => null );
        if(!passSubjects.contains(checkPass) || checkPass == null){
          tempList.removeAt(i);
          i--;
        }
      }
    }
//    print("2 " + tempList.toString() + " " + tempList.length.toString());
  }
  _checkPairSubject(List<User_timetable> tempList){
    for(int i = 0; i < tempList.length; i++){
      if(tempList[i].pair_subject != null){
        var checkPass = passSubjects.firstWhere((element) => element.subject_id == tempList[i].pair_subject, orElse:() => null );
        if(!passSubjects.contains(checkPass) || checkPass == null){
          var checkPair = tempList.firstWhere((element) => element.subject_id == tempList[i].pair_subject, orElse:() => null );
          if(!tempList.contains(checkPair) || checkPair == null){
            tempList.removeAt(i);
            i--;
          }
        }
      }
    }
  }
  _checkXXXXX(List<User_timetable> tempList, List<User_timetable> tempTermList){
    for(int i = 0; i < tempList.length; i++){
      tempTermList.removeWhere((element) => element.subject_id == tempList[i].subject_id);
    }
    List<String> checkRepeat = [];
    for(int i = 0; i < tempList.length; i++){
      if(tempList[i].subject_id.substring(0,5) == "XXXXX"){
//        print(tempList[i].subject_id);
        var checkGroupCredits = getAllGroupCredits().firstWhere((element) => element.group_id == tempList[i].group_id, orElse: () => null);
        if(checkGroupCredits != null && (checkGroupCredits.currCredits >= checkGroupCredits.all_credit)){
          passSubjects.add(tempList[i]);
          tempList.removeAt(i);
          i--;
          continue;
        }
       var tempGroup = tempList[i].group_id;
       for(int j = 0; j < tempTermList.length; j++){
         if((tempTermList[j].group_id == tempGroup) && _checkSubjectAdd(tempTermList[j], globals.allCredits[indexList]-tempList[i].credit, tempList)
         && (tempTermList[j].subject_id.substring(0,5) != "XXXXX")){
//           print(yearTerm + " " + tempList[i].toString());
           if(checkRepeat.contains(tempTermList[j].subject_id)){
             continue;
           }
           passSubjects.add(tempList[i]);
           print(tempTermList[j]);
           tempList[i] = tempTermList[j];
           checkRepeat.add(tempTermList[j].subject_id);
           break;
         }
       }
      }
    }
    List<User_timetable> tempXXXXXList = [];
    for(int i = 0; i < allTimetable.length; i++){
      if(allTimetable[i].subject_id.substring(0,5) == "XXXXX"){
        tempXXXXXList.add(allTimetable[i]);
      }
    }
   for(int j = 0; j < tempXXXXXList.length; j++){
     var checkGroupCredits = getAllGroupCredits().firstWhere((element) => element.group_id == tempXXXXXList[j].group_id, orElse: () => null);
     if(checkGroupCredits != null && (checkGroupCredits.currCredits >= checkGroupCredits.all_credit)){
         passSubjects.add(tempXXXXXList[j]);
     }
   }
  }
  bool checkAllGroupPass(List<AllGroupCredits> tempList){
    for(int i = 0; i < tempList.length; i++){
      if(tempList[i].currCredits < tempList[i].all_credit){
        return false;
      }
    }
    return true;
  }
  @override
  String toString() {
    // TODO: implement toString
    String temp = " ";
    globals.getAllCredits(indexList);
    for(int i = 0; i < planTimetable.length; i++){
      temp += planTimetable[i].subject_id + " ";
    }
    return temp + " " + yearTerm + " Credits: ${globals.allCredits[indexList]}" + "\n";
  }
}

// ignore: must_be_immutable
class TabBarTimetable extends StatefulWidget {

  int indexList;
  OverallTimetable overallTimetable;
  CurrentCredits currentCredits;

  TabBarTimetable(this.indexList, this.overallTimetable, this.currentCredits);
  @override
  _TabBarTimetableState createState() => _TabBarTimetableState(indexList, overallTimetable, currentCredits);
}

class _TabBarTimetableState extends State<TabBarTimetable> with TickerProviderStateMixin{

  final GlobalKey<FormState> _tabBar = GlobalKey<FormState>(debugLabel: '_TabbarTimetable');

  // TickerProviderStateMixin allows the fade out/fade in animation when changing the active button
  int indexList;
  List<DayTimetable> dayList = [];
  OverallTimetable overallTimetable;
  CurrentCredits currentCredits;
  _TabBarTimetableState(this.indexList, this.overallTimetable, this.currentCredits);
  // this will control the button clicks and tab changing
  TabController _controller;

  // this will control the animation when a button changes from an off state to an on state
  AnimationController _animationControllerOn;

  // this will control the animation when a button changes from an on state to an off state
  AnimationController _animationControllerOff;

  // this will give the background color values of a button when it changes to an on state
  Animation _colorTweenBackgroundOn;
  Animation _colorTweenBackgroundOff;

  // this will give the foreground color values of a button when it changes to an on state
  Animation _colorTweenForegroundOn;
  Animation _colorTweenForegroundOff;

  // when swiping, the _controller.index value only changes after the animation, therefore, we need this to trigger the animations and save the current index
  int _currentIndex = 0;

  // saves the previous active tab
  int _prevControllerIndex = 0;

  // saves the value of the tab animation. For example, if one is between the 1st and the 2nd tab, this value will be 0.5
  double _aniValue = 0.0;

  // saves the previous value of the tab animation. It's used to figure the direction of the animation
  double _prevAniValue = 0.0;

  // these will be our tab icons. You can use whatever you like for the content of your buttons
  List _days = [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
  ];
  // active button's foreground color
  Color _foregroundOn = Colors.blue;
  Color _foregroundOff = Colors.white;

  // active button's background color
  Color _backgroundOn = Colors.white;
  Color _backgroundOff = Colors.blue;

  // scroll controller for the TabBar
  ScrollController _scrollController = new ScrollController();

  // this will save the keys for each Tab in the Tab Bar, so we can retrieve their position and size for the scroll controller
  List _keys = [];

  // regist if the the button was tapped
  bool _buttonTap = false;

  @override
  void initState() {
    super.initState();
    for (int index = 0; index < _days.length; index++) {
      // create a GlobalKey for each Tab
      _keys.add(new GlobalKey());
    }

    // this creates the controller with 6 tabs (in our case)
    _controller = TabController(vsync: this, length: _days.length);
    // this will execute the function every time there's a swipe animation
    _controller.animation.addListener(_handleTabAnimation);
    // this will execute the function every time the _controller.index value changes
    _controller.addListener(_handleTabChange);

    _animationControllerOff =
        AnimationController(vsync: this, duration: Duration(milliseconds: 75));
    // so the inactive buttons start in their "final" state (color)
    _animationControllerOff.value = 1.0;
    _colorTweenBackgroundOff =
        ColorTween(begin: _backgroundOn, end: _backgroundOff)
            .animate(_animationControllerOff);
    _colorTweenForegroundOff =
        ColorTween(begin: _foregroundOn, end: _foregroundOff)
            .animate(_animationControllerOff);

    _animationControllerOn =
        AnimationController(vsync: this, duration: Duration(milliseconds: 150));
    // so the inactive buttons start in their "final" state (color)
    _animationControllerOn.value = 1.0;
    _colorTweenBackgroundOn =
        ColorTween(begin: _backgroundOff, end: _backgroundOn)
            .animate(_animationControllerOn);
    _colorTweenForegroundOn =
        ColorTween(begin: _foregroundOff, end: _foregroundOn)
            .animate(_animationControllerOn);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      key: _tabBar,
        children: <Widget>[
          // this is the TabBar
          Container(
              color: Colors.blue,
              height: 50.0,
              // this generates our tabs buttons
              child: ListView.builder(
                // this gives the TabBar a bounce effect when scrolling farther than it's size
                  physics: BouncingScrollPhysics(),
                  controller: _scrollController,
                  // make the list horizontal
                  scrollDirection: Axis.horizontal,
                  // number of tabs
                  itemCount: _days.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      // each button's key
                        key: _keys[index],
                        // padding for the buttons
                        padding: EdgeInsets.all(0.0),
                        child: ButtonTheme(
                            child: AnimatedBuilder(
                              animation: _colorTweenBackgroundOn,
                              builder: (context, child) => FlatButton(
                                // get the color of the button's background (dependent of its state)
                                  color: _getBackgroundColor(index),
                                  // make the button a rectangle with round corners
//                                  shape: RoundedRectangleBorder(
//                                      borderRadius: new BorderRadius.circular(0.0)),
                                  onPressed: () {
                                    setState(() {
                                      _buttonTap = true;
                                      // trigger the controller to change between Tab Views
                                      _controller.animateTo(index);
                                      // set the current index
                                      _setCurrentIndex(index);
                                      // scroll to the tapped button (needed if we tap the active button and it's not on its position)
                                      _scrollTo(index);
                                    });
                                  },
                                  child: Text(
                                    // get the icon
                                    _days[index],
                                    // get the color of the icon (dependent of its state)
                                    style: TextStyle(color: _getForegroundColor(index),
                                        fontSize: 15),
                                  )
                              ),
                            )));
                  })),
          Flexible(
            // this will host our Tab Views
              child: TabBarView(
                // and it is controlled by the controller
                controller: _controller,
                children: <Widget>[
                  // our Tab Views
                  globals.dayList[indexList][0],
                  globals.dayList[indexList][1],
                  globals.dayList[indexList][2],
                  globals.dayList[indexList][3],
                  globals.dayList[indexList][4],
                  globals.dayList[indexList][5],
                  globals.dayList[indexList][6],
                ],
              )),

//          CurrentCredits(_allCredits),
        ]);

  }

  // runs during the switching tabs animation
  _handleTabAnimation() {
    // gets the value of the animation. For example, if one is between the 1st and the 2nd tab, this value will be 0.5
    _aniValue = _controller.animation.value;

    // if the button wasn't pressed, which means the user is swiping, and the amount swipped is less than 1 (this means that we're swiping through neighbor Tab Views)
    if (!_buttonTap && ((_aniValue - _prevAniValue).abs() < 1)) {
      // set the current tab index
      _setCurrentIndex(_aniValue.round());
    }

    // save the previous Animation Value
    _prevAniValue = _aniValue;
  }

  // runs when the displayed tab changes
  _handleTabChange() {
    // if a button was tapped, change the current index
    if (_buttonTap) _setCurrentIndex(_controller.index);

    // this resets the button tap
    if ((_controller.index == _prevControllerIndex) ||
        (_controller.index == _aniValue.round())) _buttonTap = false;

    // save the previous controller index
    _prevControllerIndex = _controller.index;
  }

  _setCurrentIndex(int index) {
    // if we're actually changing the index
    if (index != _currentIndex) {
      setState(() {
        // change the index
        _currentIndex = index;
      });

      // trigger the button animation
      _triggerAnimation();
      // scroll the TabBar to the correct position (if we have a scrollable bar)
      _scrollTo(index);
    }
  }

  _triggerAnimation() {
    // reset the animations so they're ready to go
    _animationControllerOn.reset();
    _animationControllerOff.reset();

    // run the animations!
    _animationControllerOn.forward();
    _animationControllerOff.forward();
  }

  _scrollTo(int index) {
    // get the screen width. This is used to check if we have an element off screen
    double screenWidth = MediaQuery.of(context).size.width;

    // get the button we want to scroll to
    RenderBox renderBox = _keys[index].currentContext.findRenderObject();
    // get its size
    double size = renderBox.size.width;
    // and position
    double position = renderBox.localToGlobal(Offset.zero).dx;

    // this is how much the button is away from the center of the screen and how much we must scroll to get it into place
    double offset = (position + size / 2) - screenWidth / 2;

    // if the button is to the left of the middle
    if (offset < 0) {
      // get the first button
      renderBox = _keys[0].currentContext.findRenderObject();
      // get the position of the first button of the TabBar
      position = renderBox.localToGlobal(Offset.zero).dx;

      // if the offset pulls the first button away from the left side, we limit that movement so the first button is stuck to the left side
      if (position > offset) offset = position;
    } else {
      // if the button is to the right of the middle

      // get the last button
      renderBox = _keys[_days.length - 1].currentContext.findRenderObject();
      // get its position
      position = renderBox.localToGlobal(Offset.zero).dx;
      // and size
      size = renderBox.size.width;

      // if the last button doesn't reach the right side, use it's right side as the limit of the screen for the TabBar
      if (position + size < screenWidth) screenWidth = position + size;

      // if the offset pulls the last button away from the right side limit, we reduce that movement so the last button is stuck to the right side limit
      if (position + size - offset < screenWidth) {
        offset = position + size - screenWidth;
      }
    }

    // scroll the calculated ammount
    _scrollController.animateTo(offset + _scrollController.offset,
        duration: new Duration(milliseconds: 150), curve: Curves.easeInOut);
  }

  _getBackgroundColor(int index) {
    if (index == _currentIndex) {
      // if it's active button
      return _colorTweenBackgroundOn.value;
    } else if (index == _prevControllerIndex) {
      // if it's the previous active button
      return _colorTweenBackgroundOff.value;
    } else {
      // if the button is inactive
      return _backgroundOff;
    }
  }

  _getForegroundColor(int index) {
    // the same as the above
    if (index == _currentIndex) {
      return _colorTweenForegroundOn.value;
    } else if (index == _prevControllerIndex) {
      return _colorTweenForegroundOff.value;
    } else {
      return _foregroundOff;
    }
  }
}

// ignore: must_be_immutable
class ShowOverall extends StatelessWidget {
  OverallTimetable overallTimetable;
  Timetabling timetabling;
  ShowOverall(this.overallTimetable, this.timetabling);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => timetabling),
            );
          },
        ),
        title: Text('Overall Timetable'),
      ),
      body: overallTimetable,
    );
  }
}
