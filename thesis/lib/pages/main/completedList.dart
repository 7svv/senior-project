import 'package:flutter/material.dart';
import 'package:thesis/data/database-helper.dart';
import 'dart:async';
import 'package:thesis/models/show_status.dart';
import 'package:thesis/models/user_status.dart';
import 'package:thesis/sizeconfig.dart';

String _studentId = getUserLogged();
var vertical = SizeConfig.safeBlockVertical;
var horizontal = SizeConfig.blockSizeHorizontal;
var fontSize = SizeConfig.blockSizeVertical;

setStudentCompletedList(String student_id){
  _studentId = student_id;
}

class CompletedList extends StatefulWidget {
  CompletedList({Key key}) : super(key: key);

  @override
  _CompletedList createState() => _CompletedList();
}
class _CompletedList extends State<CompletedList> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Completed List',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
//      home: MyHomePage(),
      home: MyCurriculumList(),
//        MyPage(),
    );
  }
}

String getGroupName(String group_id) {
  String group_name;
  switch (group_id) {
    case '1.1':
      group_name = '1.1 หมวดมนุษย์ศาสตร์ (บังคับ 1 วิชา)';
      break;
    case '1.2':
      group_name = '1.2 หมวดมนุษย์ศาสตร์ (บังคับ 1 วิชา)';
      break;
    case '1.3':
      group_name = '1.3 หมวดวิทยาศาสตร์และคณิตศาสตร์';
      break;
    case '1.3.1':
      group_name = '1.3.1 วิทยาศาสตร์ (บังคับศึกษา 1 วิชา)';
      break;
    case '1.3.2':
      group_name = '1.3.2 คณิตศาสตร์หรือคอมพิวเตอร์ (บังคับศึกษา 1 วิชา)';
      break;
    case '1.4':
      group_name = '1.4 หมวดภาษา';
      break;
    case '1.4.1':
      group_name = '1.4.1 ภาษาไทย';
      break;
    case '1.4.2':
      group_name = '1.4.2 ภาษาต่างประเทศ';
      break;
    case '1.5':
      group_name = '1.5 หลักสูตรวิชาศึกษาทั่วไป (ส่วนที่ 2)';
      break;
    case '1.5.1':
      group_name = '1.5.1 - บังคับ 3 วิชา';
      break;
    case '1.5.2':
      group_name = '1.5.2 - บังคับเลือก 1 วิชา ไม่น้อยกว่า 2 หน่วกิต';
      break;
    case '2.1':
      group_name = '2.1 วิชาแกน';
      break;
    case '2.1.1':
      group_name = '2.1.1 วิชาพื้นฐานคณิตศาสตร์และวิทยาศาสตร์';
      break;
    case '2.1.2':
      group_name = '2.1.2 วิชาพื้นฐานวิศวกรรม';
      break;
    case '2.2':
      group_name = '2.2 วิชาเฉพาะด้าน';
      break;
    case '2.2.1':
      group_name = '2.2.1 - กลุ่มเทคโนโลยีเพื่องานประยุกต์';
      break;
    case '2.2.2':
      group_name = '2.2.2 - กลุ่มเทคโนโลยีและวิธีการทางซอฟต์แวร์';
      break;
    case '2.2.3':
      group_name = '2.2.3 - กลุ่มโครงสร้างพื้นฐานระบบ';
      break;
    case '2.2.4':
      group_name = '2.2.4 - กลุ่มฮาร์ดแวร์ และสถาปัตยกรรมคอมพิวเตอร์';
      break;
    case '2.3':
      group_name = '2.3 วิชาเลือก';
      break;
    case '2.3.1':
      group_name = '2.3.1 วิชาเลือกด้านการออกแบบและพัฒนา';
      break;
    case '2.3.2':
      group_name = '2.3.2 วิชาเลือกเฉพาะรูปแบบ';
      break;
    case '2.3.2.1':
      group_name = '2.3.2.1 - รุปแบบที่ 1 (1) ให้ศึกษาวิชาฝึกงาน/โครงงานทางด้านวิศวกรรมคอมพิวเตอร์';
      break;
    case '2.3.2.2':
      group_name = '2.3.2.2 - รูปแบบที่ 1 (2) เลือกศึกษาวิชาเลือกเฉพาะรูปแบบที่คณะกำหนด';
      break;
    case '2.3.2.3':
      group_name = '2.3.2.3 - รูปแบบที่ 2 (1) ให้ศึกษาวิชาสหกิจศึกษาวิศวกรรมคอมพิวเตอร์';
      break;
    case '2.3.2.4':
      group_name = '2.3.2.4 - รูปแบบที่ 2 (2) เลือกศึกษาวิชาเลือกเฉพาะรูปแบบที่คณะกำหนด';
      break;
    default:
      {
        group_name = group_id; break;
      }
  }
  return group_name;
}

Future<List<Show_status>> fetchList_byGroupFromDatabase(String group_id) async {
  var dbHelper = DatabaseHelper();
  Future<List<Show_status>> status;
  print("fetch: " +_studentId);
  status = dbHelper.getAllCompletedList_byGroup(_studentId, group_id);
  return status;
}

String getUserLogged() {
  var dbHelper = DatabaseHelper();
  String currLogged;
  Future<String> student_id;
  student_id = dbHelper.userLogged();
  student_id.then(
        (itemList) {
      currLogged = itemList;
    },
  );
  print("userLogged = " + currLogged);
  return currLogged;
}

List<bool> getAllStatus() {
  print("cur student = "+_studentId);
  var dbHelper = DatabaseHelper();
  List<bool> allStatus = [];
  Future<List<int>> tempStatus;
  tempStatus = dbHelper.getStatusList(_studentId);
  tempStatus.then(
        (itemList) {
      for(int i = 0; i < itemList.length; i++){
        bool boolStatus = false;
        if(itemList[i] == 1)
          boolStatus = true;
        allStatus.add(boolStatus);
      }
    },
  );
  return allStatus;
}

void updateStatus(User_status newStatus) {
  var dbHelper = DatabaseHelper();
  dbHelper.updateStatus(newStatus);
  print('done..');
  dbHelper.allList(_studentId);
}

class MyCurriculumList extends StatefulWidget {
  @override
  MyCurriculumListPageState createState() => new MyCurriculumListPageState();
}

class MyCurriculumListPageState extends State<MyCurriculumList> {
  List<bool> isCheck = getAllStatus();
  String preGroup;
  @override
  Widget build(BuildContext context) {

    FutureBuilder allListBuild (String groupSelector, int groupLength) {
      print("groupSelector " + groupSelector);
      return FutureBuilder<List<Show_status>>(
        future: fetchList_byGroupFromDatabase(groupSelector),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if(groupLength > 3) {
              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    String groupChanged = snapshot.data[index].group_id;
                    int groupCheck = groupChanged.length;
                    if(groupCheck > 3){
                      groupCheck = groupChanged.substring(0,3).length;
                    }
                    else{
                      groupCheck = 0;
                    }
                    print("groupChanged " + groupChanged + " groupCheck " + groupCheck.toString());
                    if (groupChanged != preGroup) {
                      preGroup = groupChanged;
                      return new ExpansionTile(
                          leading: Icon(
                            Icons.filter_1,
                            size: fontSize * 3,
                            color: Colors.pinkAccent,
                          ),
                          title: Text(
                            getGroupName(groupChanged),
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: fontSize * 2.5),
                          ),
                          children: <Widget>[
                            allListBuild(groupChanged, groupCheck)
                          ]
                      );
                    }
                    else{
                      return SizedBox.shrink();
                    }
                  }
              );
            }
            else
            {
              return new ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    print("index " + index.toString());
                    int id = snapshot.data[index].id;
                    int getStatus = snapshot.data[index].status;
                    bool status = false;
                    if (getStatus == 1) {
                      status = true;
                    }
                    print("id " + id.toString());
                    String group_id = snapshot.data[index]
                        .group_id;

                    return new Padding(
                        padding:
                        const EdgeInsets.all(
                            10),
                        child: Row(
                          crossAxisAlignment:
                          CrossAxisAlignment
                              .center,
                          children: <Widget>[
                            new Checkbox(
                                value: status,
                                onChanged:
                                    (bool value) {
                                  int checkInt;
                                  setState(() {
                                    print(
                                        status);
                                    status =
                                        value;
                                    if (status)
                                      checkInt =
                                      1;
                                    else
                                      checkInt =
                                      0;
                                    User_status currStatus = new User_status(
                                        snapshot
                                            .data[
                                        index]
                                            .student_id,
                                        snapshot
                                            .data[
                                        index]
                                            .subject_id,
                                        checkInt,
                                        snapshot
                                            .data[
                                        index]
                                            .curriculum,
                                        snapshot
                                            .data[
                                        index]
                                            .group_id);
                                    currStatus
                                        .setID(
                                        id);
                                    updateStatus(
                                        currStatus);
                                  });
                                }),
                            new Padding(
                                padding:
                                EdgeInsets.all(
                                    12),
                                child: new Text(
                                    snapshot
                                        .data[
                                    index]
                                        .subject_id,
                                    style: new TextStyle(
                                        fontWeight:
                                        FontWeight
                                            .bold,
                                        fontSize:
                                        fontSize * 2))),
                            new Flexible(
                                child: new Text(
                                    snapshot
                                        .data[
                                    index]
                                        .subject_name,
                                    style: new TextStyle(
                                        fontSize:
                                        fontSize * 2))),
                            new Divider(),
                          ],
                        ));
                  });
            }
          } else if (snapshot.hasError) {
            return new Text("${snapshot.error}");
          }
          return new Container(
              alignment: AlignmentDirectional.center,
              padding: EdgeInsets.all(20),
//                        child: new CircularProgressIndicator(),
              child: new Text('ไม่พบข้อมูลรายวิชา',
                  style: new TextStyle(fontSize: 20.0)));
        },
      );
    }

    print("Alllist = " + _studentId);
    var CompletedListForm = new Column(
      children: [
        new Padding(
          padding: new EdgeInsets.all(horizontal*5),
          child: ExpansionTile(
            leading: Icon(
              Icons.filter_1,
              size: fontSize*4,
              color: Colors.pinkAccent,
            ),
            title: Text(
              'หลักสูตรวิชาศึกษาทั่วไป',
              style:
              new TextStyle(fontWeight: FontWeight.bold, fontSize: fontSize*3),
            ),
            children: <Widget>[
              new FutureBuilder<List<Show_status>>(
                future: fetchList_byGroupFromDatabase('1'),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return new ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          String currGroup = snapshot.data[index].group_id;
                          String parentGroup = currGroup.substring(0,3);
                          int currGroupLength = currGroup.length;
                          print("currGroup = " + currGroup);

                          if (parentGroup != preGroup) {
                            preGroup = parentGroup;
                            return new ExpansionTile(
                                leading: Icon(
                                  Icons.filter_1,
                                  size: fontSize * 3,
                                  color: Colors.pinkAccent,
                                ),
                                title: Text(
                                  getGroupName(parentGroup),
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: fontSize * 2.5),
                                ),
                                children: <Widget>[
                                  allListBuild(parentGroup, currGroupLength)
                                ]
                            );
                          }
                          return SizedBox.shrink();
                        });
                  } else if (snapshot.hasError) {
                    return new Text("${snapshot.error}");
                  }
                  return new Container(
                      alignment: AlignmentDirectional.center,
                      padding: EdgeInsets.all(20),
//                        child: new CircularProgressIndicator(),
                      child: new Text('ไม่พบข้อมูลรายวิชา',
                          style: new TextStyle(fontSize: fontSize * 3)));
                },
              ),
            ],
          ),
        ),
        new Padding(
          padding: new EdgeInsets.all(20.0),
//                  scrollDirection: Axis.vertical,
          child: ExpansionTile(
            leading: Icon(
              Icons.filter_2,
              size: fontSize*4,
              color: Colors.pinkAccent,
            ),
            title: Text(
              'วิชาเฉพาะ',
              style:
              new TextStyle(fontWeight: FontWeight.bold, fontSize: fontSize*3),
            ),
            children: <Widget>[
              new FutureBuilder<List<Show_status>>(
                future: fetchList_byGroupFromDatabase('2'),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return new ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          String currGroup = snapshot.data[index].group_id;
                          String parentGroup = currGroup.substring(0,3);
                          int currGroupLength = currGroup.length;
                          print("currGroup = " + currGroup);

                          if (parentGroup != preGroup) {
                            preGroup = parentGroup;
                            return new ExpansionTile(
                                leading: Icon(
                                  Icons.filter_1,
                                  size: fontSize * 3,
                                  color: Colors.pinkAccent,
                                ),
                                title: Text(
                                  getGroupName(parentGroup),
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: fontSize * 2.5),
                                ),
                                children: <Widget>[
                                  allListBuild(parentGroup, currGroupLength)
                                ]
                            );
                          }
                          return SizedBox.shrink();
                        });
                  } else if (snapshot.hasError) {
                    return new Text("${snapshot.error}");
                  }
                  return new Container(
                      alignment: AlignmentDirectional.center,
                      padding: EdgeInsets.all(20),
//                        child: new CircularProgressIndicator(),
                      child: new Text('ไม่พบข้อมูลรายวิชา',
                          style: new TextStyle(fontSize: fontSize * 3)));
                },
              ),
            ],
          ),
        ),
        new Padding(
          padding: new EdgeInsets.all(20.0),
//                  scrollDirection: Axis.vertical,

          child: ExpansionTile(
            leading: Icon(
              Icons.filter_3,
              size: fontSize*4,
              color: Colors.pinkAccent,
            ),
            title: Text(
              'วิชาเลือกเสรี',
              style:
              new TextStyle(fontWeight: FontWeight.bold, fontSize: fontSize*3),
            ),
            children: <Widget>[
              new FutureBuilder<List<Show_status>>(
                future: fetchList_byGroupFromDatabase('3'),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return new ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          String currGroup = snapshot.data[index].group_id;
                          String parentGroup = currGroup.substring(0,3);
                          int currGroupLength = currGroup.length;
                          print("currGroup = " + currGroup);

                          if (parentGroup != preGroup) {
                            preGroup = parentGroup;
                            return new ExpansionTile(
                                leading: Icon(
                                  Icons.filter_1,
                                  size: fontSize * 3,
                                  color: Colors.pinkAccent,
                                ),
                                title: Text(
                                  getGroupName(parentGroup),
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: fontSize * 2.5),
                                ),
                                children: <Widget>[
                                  allListBuild(parentGroup, currGroupLength)
                                ]
                            );
                          }
                          return SizedBox.shrink();
                        });
                  } else if (snapshot.hasError) {
                    return new Text("${snapshot.error}");
                  }
                  return new Container(
                      alignment: AlignmentDirectional.center,
                      padding: EdgeInsets.all(20),
//                        child: new CircularProgressIndicator(),
                      child: new Text('ไม่พบข้อมูลรายวิชา',
                          style: new TextStyle(fontSize: fontSize * 3)));
                },
              ),
            ],
          ),
        ),
      ],
    );

    return new Scaffold(
        body: SingleChildScrollView(
          child: new Center(
            child: CompletedListForm,
          ),
        )
    );
  }
}
