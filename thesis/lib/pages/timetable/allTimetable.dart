import 'package:flutter/material.dart';
import 'package:thesis/models/timetable.dart';
import 'package:thesis/pages/main/menuList.dart';
import 'package:thesis/pages/timetable/timetabling.dart';
import 'package:thesis/sizeconfig.dart';
import 'package:thesis/models/global_vars.dart' as globals;

String _yearTerm;
int _year;
int _term;

setYearTermAllTimetable(String yearTerm) {
  _yearTerm = yearTerm;
  _year = int.parse(yearTerm.substring(0, 1));
  _term = int.parse(yearTerm.substring(2, 3));
}

class AllTimetable extends StatefulWidget {
  String currYearTerm;
  List<Timetable> termTimetable;

  AllTimetable(this.currYearTerm, this.termTimetable);

  @override
  _AllTimetablePageState createState() =>
      _AllTimetablePageState(currYearTerm, termTimetable);
}

class _AllTimetablePageState extends State<AllTimetable> {
  String currYearTerm;
  List<Timetable> termTimetable;

  _AllTimetablePageState(this.currYearTerm, this.termTimetable);

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    isLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Timetable"),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MenuList()),
            );
          },
        ),
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: termTimetable.length,
            itemBuilder: (context, index) {
              var allTermCredits;
              if(index != 0){
                termTimetable[index].setOverall(termTimetable[index].planTimetable);
                allTermCredits = termTimetable[index].allCredits;
              }
              else{
                allTermCredits = globals.allCredits[index];
              }
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      "Year: " +
                          termTimetable[index].yearTerm.substring(0, 1) +
                          " Term: " +
                          termTimetable[index].yearTerm.substring(2, 3) +
                          " | Credits: " +
                          allTermCredits.toString(),
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(0),
                    child: termTimetable[index].overallTimetableTable,
                  ),
                ],
              );
            }),
      ),
    );
  }
}
