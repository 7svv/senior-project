library thesis.globals;

import 'package:flutter/cupertino.dart';
import 'package:thesis/models/credit_group.dart';
import 'package:thesis/models/current_credits.dart';
import 'package:thesis/models/day_timetable.dart';
import 'package:thesis/models/timetable.dart';
import 'package:thesis/models/user_timetable.dart';

Timetable termTimetable;
List<User_timetable> allTimetable = [];
List<int> allCredits = [];
List<List<DayTimetable>> dayList = [];
List<CurrentCredits> allCurrentCredits = [];
List<AllGroupCredits> allGroupCredits = [];

setInitTimetable(Timetable init){
  termTimetable = init;
}
setTimetableChanged(int index){
  termTimetable.dayList = dayList[index];
  termTimetable.allGroupCredits = allGroupCredits;
}
setGroupCredits(List<AllGroupCredits> tempGroupCredits){
  allGroupCredits = tempGroupCredits;
}
setAllCredits(int index, String day, List<User_timetable> tempDropdown){
  for(int i = 0; i < dayList[index].length; i++){
    if(dayList[index][i].day == day){
      dayList[index][i].dropdownValue = tempDropdown;
    }
  }
  getAllCredits(index);
}
getAllCredits(int index){
  allCredits[index] = 0;
  List<String> checkRepeat = [];
    for(int i = 0; i < dayList[index].length; i++){
      var subjectSelected = dayList[index][i].dropdownValue;
      for(int j = 0; j < subjectSelected.length; j++){
        if(!checkRepeat.contains(subjectSelected[j].subject_id)){
          allCredits[index] += (subjectSelected[j].credit != null ? subjectSelected[j].credit:0) ;
          checkRepeat.add(subjectSelected[j].subject_id);
        }
      }
    }

  allCurrentCredits[index] = new CurrentCredits(UniqueKey(), allCredits[index]);
}

setDropdownVal(int index, String day, List<User_timetable> tempDropdown){
  for(int i = 0; i < dayList[index].length; i++){
    if(dayList[index][i].day == day){
      dayList[index][i].dropdownValue = tempDropdown;
    }
  }
  getAllCredits(index);
}
List<User_timetable> getDropdownVal(int index, String day){
  for(int i = 0; i < dayList[index].length; i++){
    if(dayList[index][i].day == day){
      return dayList[index][i].dropdownValue;
    }
  }
  return null;
}

List<User_timetable> getAllDropdownVal(int index){
  List<User_timetable> tempList = [];
  for(int i = 0; i < dayList[index].length; i++){
    for(int j = 0; j < dayList[index][i].dropdownValue.length; j++){
      if(dayList[index][i].dropdownValue[j].subject_id != "None"){
        tempList.add(dayList[index][i].dropdownValue[j]);
      }
    }
  }
  return tempList;
}