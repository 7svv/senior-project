import 'package:flutter/material.dart';
import 'package:thesis/data/database-helper.dart';
import 'package:thesis/models/user.dart';
import 'package:thesis/pages/login/login_page.dart';
import 'package:thesis/sizeconfig.dart';
import 'password_field.dart';
import 'package:password/password.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState  extends State<RegisterPage> {
  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _name, _student_id, _password;
  bool check_pass = false;
  bool exist = false;
//  bool checkID = true;
  // Initially password is obscure
  bool _obscureText = true;
  var vertical = SizeConfig.safeBlockVertical;
  var horizontal = SizeConfig.blockSizeHorizontal;
  var fontSize = SizeConfig.blockSizeVertical;

  Future<bool> checkStudentID(String student_id) async {
    var db = await DatabaseHelper();
    Future<bool> check = db.checkUser(student_id);
    return check;
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var loginBtn = new RaisedButton(
      padding: EdgeInsets.fromLTRB(horizontal*30,vertical*2,horizontal*30,vertical*2),
      onPressed: _submit,
      child: new Text(
          "CONFIRM",
          style: TextStyle(fontSize: fontSize*3)
      ),
      color: Colors.deepOrange,
      textColor: Colors.white,
      highlightColor: Color(0xff8C2500),
      shape: StadiumBorder(),
      elevation: 8,
      highlightElevation: 2,
    );


    var loginForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
//        new Text(
//          "Create an Account",
//          textScaleFactor: 2.0,
//        ),
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.fromLTRB(horizontal*10,horizontal*10,horizontal*10,horizontal*7),
                child: new TextFormField(
                  onSaved: (val) {
                    _student_id = val;
                  },
                  onChanged: (String value){
                    var db = DatabaseHelper();
                    Future<bool> check = db.checkUser(value);
                    check.then((value){
                      if(value == true)
                        exist = true;
                      else
                        exist = false;
                    });
                  },
                  style: TextStyle(
                  color: Color(0xff1372FF),
                  fontSize: fontSize*2.5,
                ),
                  decoration: new InputDecoration(
                      labelText: "Student ID",
                      prefixIcon: new Icon(Icons.person_add,
                          size: fontSize*3.5)
                  ),
                  validator: validStudentID,
                ),
              ),
              new PasswordField(onChanged: (value) {
                  setState(() {
                    _password = value;
                  });
//                  print(_password.length);
                  if(_password.length > 0)
                    check_pass = true;
                  else
                    check_pass = false;
                }, onSaved: (value) {
                  setState(() {
                    _password = value;
                  });
                }
                ),

              new Padding(
                padding: EdgeInsets.fromLTRB(horizontal*10,0,horizontal*10,horizontal*7),
                child: new TextFormField(
                  style: TextStyle(
                    color: Color(0xff1372FF),
                    fontSize: fontSize*2.5,
                  ),
                  onSaved: (val) => val,
                  decoration: new InputDecoration(
                      labelText: "Confirm Password",
                      suffixIcon: new GestureDetector(
                        onTap: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                        child:
                        new Icon(_obscureText ? Icons.visibility : Icons.visibility_off,
                            size: fontSize*3.5),
                      ),
                      prefixIcon: new Icon(Icons.lock,
                          size: fontSize*3.5)
                  ),
                  validator: (String value) {
                    if (value.trim().isEmpty) {
                      return '';
                    }
                    else if (value != _password) {
                      return 'Password do not match!';
                    }
                    else
                      return null;
                  },
                  autofocus: false,
                  obscureText: _obscureText,
                ),
              ),
              new Padding(
                padding: EdgeInsets.fromLTRB(horizontal*10,horizontal*2,horizontal*10,horizontal*20),
                child: new TextFormField(
                  style: TextStyle(
                    color: Color(0xff1372FF),
                    fontSize: fontSize*2.5,
                  ),
                  onSaved: (val) => _name = val,
                  decoration: new InputDecoration(
                      labelText: "Name",
                      prefixIcon: new Icon(Icons.person_pin,
                      size: fontSize*3.5,)
                  ),
                  validator: (String value) {
                    if (value.trim().isEmpty) {
                      return 'Name can not be empty!';
                    }
                    else
                       return null;
                  },
                ),
              ),
            ],
          ),
        ),
        loginBtn,
       new Padding(
         padding: EdgeInsets.fromLTRB(0,0,0,20),
       )
      ],
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("CREATE NEW USER"),
        backgroundColor: Color(0xff1372FF),
      ),
      resizeToAvoidBottomPadding: false,
      key: scaffoldKey,
      body: SingleChildScrollView(
        child: new Center(
          child: loginForm,
        ),
      )
    );
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  void _submit(){
    final algorithm = PBKDF2();
    final form = formKey.currentState;
      if (form.validate()) {
        setState(() {
          _isLoading = true;
          form.save();
          var hash = Password.hash(_password, algorithm);
          var user = new User(_name, _student_id, hash, null);
          var db = new DatabaseHelper();
          db.saveUser(user);
          _isLoading = false;
          Navigator.push(context, new MaterialPageRoute(
              builder: (context) =>
              new LoginPage())
          );
        });
      }
  }

  String validStudentID(String value) {
    String pattern = r'\+?[0-9]{10}$';
    RegExp regExp = new RegExp(pattern);
    print(value.length);
    if (value.length == 0) {
      return 'Student ID is required.';
    }
    else if (!regExp.hasMatch(value) || value.length != 10) {
      return 'Student ID should be number 0-9 and 10 digit.';
    }
    else if(checkLuhn(value)) {
      if (exist == true){
        return 'This Student ID is not available.';
      }
      return null;
    }
    else {
      return 'Please enter the correct Student ID.';
    }
  }
  bool checkLuhn(String stuNum) {
    int nDigits = stuNum.length;
    int nSum = 0;
    for (int i = nDigits - 1; i >= 0; i--) {
      int d = int.parse(stuNum[nDigits - i - 1]);
      if (i % 2 == 1) {
        d *= 2;
      }
      nSum += d > 9 ? (d - 9) : d;
      // if()
    }
    return nSum % 10 == 0;
  }
}