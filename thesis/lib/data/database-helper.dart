import 'dart:io';
import 'package:flutter/services.dart';
import 'package:thesis/models/credits.dart';
import 'package:thesis/models/curriculums.dart';
import 'package:thesis/models/credit_group.dart';
import 'package:thesis/models/show_status.dart';
import 'package:thesis/models/subjects.dart';
import 'package:thesis/models/user.dart';
import 'package:path/path.dart';
import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:thesis/models/user_status.dart';
import 'dart:typed_data';

import 'package:thesis/models/user_timetable.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;
  static Database _db;
  final String tableCurriculum = "Curriculums";
  final String tableUser = "Users";
  final String tableUserStatus = "User_status";
  final String tableSubject = "Subjects";
  final String tableSection = "Sections";
  final String tableCredit = "Credits";
  final String columnName = "name";
  final String columnStudentId = "student_id";
  final String columnPassword = "password";
  final String columnFlag = "flaglogged";
  final String columnSubjectId = "subject_id";
  final String columnStatus = "status";
  final String columnCurriculum = "curriculum";
  final String columnGroupId = "group_id";
  final String columnCredit = "credit";
  final String columnYearTerm = "year_term";
  final String columnPairSubject = "pair_subject";
  final String columnPreSubject = "pre_subject";
  final String columnSectionId = "section_id";
  final String columnSemester = "semester";
  final String columnDay = "day";
  final String columnTimeStart = "time_start";
  final String columnTimeEnd = "time_end";

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
//    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    var dbDir = await getDatabasesPath();
    String dbPath = join(dbDir, "mydatabase.db");
//    var dbDir = await getDatabasesPath();
//    var dbPath = join(dbDir, "mydatabase.db");
    print("dbPath: " + dbPath);
//    print("path: "+ path);
    var exists = await databaseExists(dbPath);
    if (!exists) {
      // Should happen only the first time you launch your application
      print("Creating new copy from asset");

      // Make sure the parent directory exists
      try {
        await Directory(dirname(dbPath)).create(recursive: true);
      } catch (_) {}

      // Copy from asset
      ByteData data = await rootBundle.load(join("assets", "spa_db.db"));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Write and flush the bytes written
      await File(dbPath).writeAsBytes(bytes, flush: true);
      print(File(dbPath));
    } else {
      print("Opening existing database");
    }
    var db = await openDatabase(dbPath);
    return db;
  }

  //Show all users
  Future<List> allUser() async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    print(dbPath);
    var dbClient = await openDatabase(dbPath);
//    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery("SELECT * FROM $tableUser");
    print("All User : ");
    if (list.length > 0) {
      list.forEach((row) {
        print(row);
      });
    } else {
      print("Users Empty.");
    }
    return list.toList();
  }

  //show all list in database
  Future<List> allList(String student_id) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
//    List<Map<String, dynamic>> list = await db.rawQuery('SELECT * FROM Users');
    List<Map<String, dynamic>> list = await dbClient.rawQuery(
        'SELECT * FROM (SELECT Curriculums.subject_id, Curriculums.subject_name, Curriculums.group_id, $tableUserStatus.student_id, $tableUserStatus.status FROM $tableUserStatus INNER JOIN $tableUserStatus ON Curriculums.subject_id = $tableUserStatus.subject_id WHERE $tableUserStatus.student_id = $student_id)');
    return list.toList();
  }

  //get all list of curriculums bt group
  Future<List<Show_status>> getAllList_byGroup(
      String student_id, String group_id) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map<String, dynamic>> list;
    if (group_id != 'all') {
      list = await dbClient.rawQuery(
//          "SELECT Curriculums.subject_id, Curriculums.group_id, Users.student_id FROM Curriculums INNER JOIN Users ON Curriculums.subject_id = Users.subject_id WHERE Users.student_id = $_studentId"
          "SELECT * FROM (SELECT $tableUserStatus.id, $tableUserStatus.student_id, Curriculums.subject_id,"
              " Curriculums.subject_name, Curriculums.curriculum, Curriculums.group_id, $tableUserStatus.status"
              " FROM Curriculums INNER JOIN $tableUserStatus ON Curriculums.subject_id = $tableUserStatus.subject_id"
              " WHERE $tableUserStatus.student_id = $student_id) WHERE group_id LIKE '$group_id%' ORDER BY group_id ASC");
    } else if (group_id == 'all') {
      list = await dbClient.rawQuery("SELECT * FROM $tableUserStatus");
    }
    List<Show_status> status = new List();
    for (int i = 0; i < list.length; i++) {
      status.add(new Show_status(
        list[i]['id'],
        list[i]['student_id'].toString(),
        list[i]['subject_id'],
        list[i]['subject_name'],
        list[i]['curriculum'],
        list[i]['group_id'],
        list[i]['status'],
      ));
    }
    print(status.length);
    if (status.length < 1) {
      print('nothing');
      return null;
    }
    return status;
  }

  Future<List<Show_status>> getAllCompletedList_byGroup(
      String student_id, String group_id) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map<String, dynamic>> list;
    if (group_id != 'all') {
      list = await dbClient.rawQuery(
//          "SELECT Curriculums.subject_id, Curriculums.group_id, Users.student_id FROM Curriculums INNER JOIN Users ON Curriculums.subject_id = Users.subject_id WHERE Users.student_id = $_studentId"
          "SELECT * FROM (SELECT $tableUserStatus.id, $tableUserStatus.student_id,"
              " Curriculums.subject_id, Curriculums.subject_name, Curriculums.curriculum,"
              " Curriculums.group_id, $tableUserStatus.status FROM Curriculums"
              " INNER JOIN $tableUserStatus ON Curriculums.subject_id = $tableUserStatus.subject_id"
              " WHERE $tableUserStatus.student_id = $student_id AND $tableUserStatus.status = 1)"
              " WHERE group_id LIKE '$group_id%' ORDER BY group_id ASC");
    } else if (group_id == 'all') {
      list = await dbClient.rawQuery("SELECT * FROM $tableUserStatus");
    }
    List<Show_status> status = new List();
    for (int i = 0; i < list.length; i++) {
      status.add(new Show_status(
        list[i]['id'],
        list[i]['student_id'].toString(),
        list[i]['subject_id'],
        list[i]['subject_name'],
        list[i]['curriculum'],
        list[i]['group_id'],
        list[i]['status'],
      ));
    }
    print(status.length);
    if (status.length < 1) {
      print('nothing');
      return null;
    }
    return status;
  }

  Future<List<Show_status>> getAllInCompletedList_byGroup(
      String student_id, String group_id) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map<String, dynamic>> list;
    if (group_id != 'all') {
      list = await dbClient.rawQuery(
//          "SELECT Curriculums.subject_id, Curriculums.group_id, Users.student_id FROM Curriculums INNER JOIN Users ON Curriculums.subject_id = Users.subject_id WHERE Users.student_id = $_studentId"
          "SELECT * FROM (SELECT $tableUserStatus.id, $tableUserStatus.student_id,"
              " Curriculums.subject_id, Curriculums.subject_name, Curriculums.curriculum,"
              " Curriculums.group_id, $tableUserStatus.status FROM Curriculums"
              " INNER JOIN $tableUserStatus ON Curriculums.subject_id = $tableUserStatus.subject_id"
              " WHERE $tableUserStatus.student_id = $student_id AND $tableUserStatus.status = 0)"
              " WHERE group_id LIKE '$group_id%' ORDER BY group_id ASC");
    } else if (group_id == 'all') {
      list = await dbClient.rawQuery("SELECT * FROM $tableUserStatus");
    }
    List<Show_status> status = new List();
    for (int i = 0; i < list.length; i++) {
      status.add(new Show_status(
        list[i]['id'],
        list[i]['student_id'].toString(),
        list[i]['subject_id'],
        list[i]['subject_name'],
        list[i]['curriculum'],
        list[i]['group_id'],
        list[i]['status'],
      ));
    }
    print(status.length);
    if (status.length < 1) {
      print('nothing');
      return null;
    }
    return status;
  }

  Future<List<int>> getStatusList(String student_id) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);

    List<Map<String, dynamic>> list = await dbClient.rawQuery(
        "SELECT id, status FROM $tableUserStatus WHERE student_id = $student_id");

    List<int> checkList = [];
    for (int i = 0; i < list.length; i++) {
//      print('id = '+ list[i]['id'].toString() + ' st = '+ list[i]['status'].toString());
      checkList.add(
        list[i]['status'],
      );
    }
    return checkList;
  }

  getCurr() async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var db = await openDatabase(dbPath);
    List<Map<String, dynamic>> list =
        await db.rawQuery("SELECT * FROM Curriculums");
    print(list);
  }

  Future<void> updateStatus(User_status newStatus) async {
    // Get a reference to the database.
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);

    print('status = ' + newStatus.status.toString());
    var res = await dbClient.update(tableUserStatus, newStatus.toMap(),
        where: "id = ?", whereArgs: [newStatus.id]);
    print('status = ' + newStatus.status.toString());
    return res;
  }

  //set initial status of all subject after register
  initAllStatus(String student_id) async {
    print("Status setting...");

    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map<String, dynamic>> list;
    list = await dbClient
        .rawQuery("SELECT * FROM Curriculums WHERE curriculum = 2556");
    String currTemp;
    int status;
    String curriculum;
    String group_id;
    User_status newStatus;
    list.forEach((row) => {
          currTemp = row['subject_id'],
          status = 0,
          curriculum = '2556',
          group_id = row['group_id'],
          newStatus = new User_status(
              student_id, currTemp, status, curriculum, group_id),
          setStatus(newStatus),
        });
  }

  //insertion status
  Future<int> setStatus(User_status newStatus) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
//    Database db = await openDatabase('curriculum.db');
    int res = await dbClient.insert(tableUserStatus, newStatus.toMap());
    List<Map> list;
    list = await dbClient.rawQuery("SELECT * FROM $tableUserStatus");
//    print(list);
    return res;
  }

  Future<bool> checkUser(String student_id) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    print(student_id);
    List<Map> maps = await dbClient.query(tableUser,
        columns: [columnStudentId],
        where: "$columnStudentId = ?",
        whereArgs: [student_id]);
    print(maps);
    if (maps.length > 0) {
      print("User Exist !!!");
      return true;
    } else {
      return false;
    }
  }

  Future<String> userLogged() async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map> maps = await dbClient.query(tableUser,
        columns: [columnStudentId, columnFlag],
        where: "$columnFlag = ?",
        whereArgs: ["logged"]);
    print(maps);
    if (maps.length > 0) {
      print(maps.first['student_id'] + " Logged !!!");
      return maps.first['flaglogged'];
    } else {
      return null;
    }
  }

  //insertion
  Future<int> saveUser(User user) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    print(user.name);
    int res = await dbClient.insert(tableUser, user.toMap());
    initAllStatus(user.student_id);
    print("Save status = " + res.toString());
    print("Save User : " + user.student_id);
    return res;
  }

  //deletion
  Future<int> deleteUser(User user) async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    int res = await dbClient.delete(tableUser);
    return res;
  }

  Future<User> getUser(String student_id) async {
    print("Select User");
    print(student_id);
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map> maps = await dbClient.query(tableUser,
        columns: [columnName, columnStudentId, columnPassword],
        where: "$columnStudentId = ?",
        whereArgs: [student_id]);
    print(maps);
    if (maps.length > 0) {
      print("User Exist !!!");
      User existUser = new User(maps.first['name'], maps.first['student_id'],
          maps.first['password'], null);
      return existUser;
    } else {
      return null;
    }
  }

  Future<void> update(User user) async {
    // Get a reference to the database.
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    var res = await dbClient.update(tableUser, user.toMap(),
        where: "student_id = ?", whereArgs: [user.student_id]);
    return res;
  }

  Future<void> logout(String student_id) async {
    // Get a reference to the database.
    print(student_id);
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map> maps = await dbClient.query(tableUser,
        columns: [columnName, columnStudentId, columnPassword, columnFlag],
        where: "$columnStudentId = ?",
        whereArgs: [student_id]);
    print(maps);
    if (maps.length > 0) {
      User currentUser = new User(maps.first['name'], maps.first['student_id'],
          maps.first['password'], null);
      var res = await dbClient.update(tableUser, currentUser.toMap(),
          where: "student_id = ?", whereArgs: [currentUser.student_id]);
      return res;
    }
  }

  Future<List<User_timetable>> getTimetable(
      String student_id, int semester) async {
    print("Timetabling Begin...");
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map<String, dynamic>> list;
    if (semester != 0) {
      list = await dbClient.rawQuery(
          "SELECT * FROM (SELECT * FROM $tableCurriculum INNER JOIN $tableUserStatus ON $tableCurriculum.subject_id = $tableUserStatus.subject_id "
          "WHERE $tableUserStatus.student_id = $student_id) AS t1 "
          "INNER JOIN "
          "(SELECT * FROM $tableSubject LEFT JOIN $tableSection ON $tableSubject.section_id = $tableSection.section_id "
          "WHERE $tableSubject.semester = $semester) AS t2 "
          "ON "
          "t1.subject_id = t2.subject_id "
          "ORDER BY section_id ASC");
    } else {
      list = await dbClient.rawQuery(
          "SELECT * FROM "
          "((SELECT * FROM $tableCurriculum INNER JOIN $tableUserStatus ON $tableCurriculum.subject_id = $tableUserStatus.subject_id "
          "WHERE $tableUserStatus.student_id = $student_id) AS t1 "
          "LEFT JOIN "
          "(SELECT * FROM $tableCurriculum LEFT JOIN $tableCredit ON $tableCurriculum.group_id = $tableCredit.group_id) AS t0 "
          "ON t1.subject_id = t0.subject_id )"
          "INNER JOIN "
          "(SELECT * FROM $tableSubject LEFT JOIN $tableSection ON $tableSubject.section_id = $tableSection.section_id) AS t2 "
          "ON "
          "t1.subject_id = t2.subject_id "
          "ORDER BY section_id ASC");
//      list = await dbClient.rawQuery(
//          "SELECT * FROM $tableSubject LEFT JOIN $tableSection ON $tableSection.section_id = $tableSubject.section_id "
//              "WHERE $tableSubject.subject_id = 'ME100' "
//      );
//      print(list);
    }
    List<User_timetable> timetableList = new List();
    for (int i = 0; i < list.length; i++) {
//      print(list[i]['subject_id']);
      timetableList.add(new User_timetable(
          list[i]['status'],
          list[i]['subject_id'],
          list[i]['subject_name'],
          list[i]['credit'],
          list[i]['group_id'],
          list[i]['name_group'],
          list[i]['all_credit'],
          list[i]['year_term'],
          list[i]['pair_subject'],
          list[i]['pre_subject'],
          list[i]['section_id'] != null ? list[i]['section_id'] : "???",
          list[i]['semester'],
          list[i]['day'] != null ? list[i]['day'] : "???",
          list[i]['time_start'] != null ? list[i]['time_start'] : "???",
          list[i]['time_end'] != null ? list[i]['time_end'] : "???"));
    }
    print(timetableList.length);
    if (timetableList.length < 1) {
      print('nothing');
      return null;
    }
    return timetableList;
  }

  Future<List<AllGroupCredits>> getAllCreditGroups() async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map<String, dynamic>> list =
        await dbClient.rawQuery('SELECT * FROM Credits');
    List<AllGroupCredits> timetableList = new List();
    for (int i = 0; i < list.length; i++) {
      timetableList.add(new AllGroupCredits(
        list[i]['group_id'],
        list[i]['name_group'],
        list[i]['all_credit'],
        list[i]['curriculum'],
      ));
    }
    print(timetableList.length);
    if (timetableList.length < 1) {
      print('nothing');
      return null;
    }
    return timetableList;
  }

  Future<List<Credits>> getCredits() async {
    var dbDir = await getDatabasesPath();
    var dbPath = join(dbDir, "mydatabase.db");
    var dbClient = await openDatabase(dbPath);
    List<Map<String, dynamic>> list;
    list = await dbClient.rawQuery("SELECT * FROM Credits");
    List<Credits> creditsList = new List();
    for (int i = 0; i < list.length; i++) {
      if (list[i] != null) {
        creditsList.add(new Credits(
          list[i]['group_id'],
          list[i]['name_group'],
          list[i]['all_credit'],
          list[i]['curriculum'],
        ));
      }
      print(creditsList.length);
      if (creditsList.length < 1) {
        print('nothing');
        return null;
      }
      return creditsList;
    }
  }

    Future<List<Curriculums>> getCurriculums() async {
      var dbDir = await getDatabasesPath();
      var dbPath = join(dbDir, "mydatabase.db");
      var dbClient = await openDatabase(dbPath);
      List<Map<String, dynamic>> list;
      list = await dbClient.rawQuery("SELECT * FROM Curriculums");
      List<Curriculums> curriculumList = new List();
      for (int i = 0; i < list.length; i++) {
        if (list[i] != null) {
          curriculumList.add(new Curriculums(
            list[i]['subject_id'],
            list[i]['subject_name'],
            list[i]['credit'],
            list[i]['group_id'],
            list[i]['year_term'],
            list[i]['curriculum'],
            list[i]['pair_subject'],
            list[i]['pre_subject'],
          ));
        }
      }
      print(curriculumList.length);
      if (curriculumList.length < 1) {
        print('nothing');
        return null;
      }
      return curriculumList;
    }

    Future<List<Subjects>> getSubjects() async {
      var dbDir = await getDatabasesPath();
      var dbPath = join(dbDir, "mydatabase.db");
      var dbClient = await openDatabase(dbPath);
      List<Map<String, dynamic>> list;
      list = await dbClient.rawQuery("SELECT * FROM Subjects");
      List<Subjects> subjectList = new List();
      for (int i = 0; i < list.length; i++) {
        if (list[i] != null) {
          subjectList.add(new Subjects(
            list[i]['id'],
            list[i]['subject_id'],
            list[i]['section_id'],
            list[i]['semester'],
          ));
        }
      }
      print(subjectList.length);
      if (subjectList.length < 1) {
        print('nothing');
        return null;
      }
      return subjectList;
    }
}
